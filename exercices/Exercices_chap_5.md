### Exercices : Modules, Bibliothèques et Projets

Ces exercices sont conçus pour initier les débutants à l'utilisation des modules, des bibliothèques externes et à la création de projets simples en Python.
Ces exercices supplémentaires permettront aux débutants de continuer à pratiquer et à renforcer leurs compétences en Python, tout en travaillant sur des projets pratiques et pertinents.

#### Exercice 1 : Utilisation de modules intégrés

**Objectif** : Apprendre à utiliser des modules intégrés de Python comme `math`, `datetime`, et `os`.

1. **Calculs mathématiques avec `math`**
   - Importez le module `math`.
   - Calculez la racine carrée de 25.
   - Calculez le sinus de 90 degrés (radian).
   - Affichez la valeur de π.

2. **Manipulation de dates avec `datetime`**
   - Importez le module `datetime`.
   - Affichez la date et l'heure actuelles.
   - Créez une date pour le 1er janvier 2020.
   - Affichez le jour de la semaine pour cette date.

3. **Gestion de fichiers avec `os`**
   - Importez le module `os`.
   - Affichez le répertoire de travail actuel.
   - Changez le répertoire de travail.
   - Créez un nouveau dossier nommé "Exercice".

#### Exercice 2 : Création de modules personnalisés

**Objectif** : Apprendre à créer et utiliser vos propres modules en Python.

1. **Module `calculs.py`**
   - Créez un fichier nommé `calculs.py`.
   - Définissez une fonction `addition(a, b)` qui retourne la somme de `a` et `b`.
   - Définissez une fonction `multiplication(a, b)` qui retourne le produit de `a` et `b`.

2. **Script principal**
   - Créez un fichier `main.py`.
   - Importez le module `calculs`.
   - Utilisez les fonctions `addition` et `multiplication` avec des valeurs de votre choix et affichez les résultats.

```python
# calculs.py
def addition(a, b):
    return a + b

def multiplication(a, b):
    return a * b

# main.py
import calculs

print(calculs.addition(3, 5))
print(calculs.multiplication(4, 7))
```

#### Exercice 3 : Utilisation de bibliothèques externes

**Objectif** : Apprendre à installer et utiliser des bibliothèques externes avec `pip`.

1. **Installation de `requests`**
   - Utilisez `pip` pour installer la bibliothèque `requests`.
   - Importez `requests`.
   - Faites une requête GET vers `https://api.github.com` et affichez le statut de la réponse.

2. **Analyse de données avec `pandas`**
   - Utilisez `pip` pour installer la bibliothèque `pandas`.
   - Importez `pandas`.
   - Créez un DataFrame à partir d'un dictionnaire et affichez-le.

3. **Création de graphiques avec `matplotlib`**
   - Utilisez `pip` pour installer la bibliothèque `matplotlib`.
   - Importez `matplotlib.pyplot`.
   - Créez un graphique simple montrant la courbe `y = x^2` pour `x` de 0 à 10.

```python
# Utilisation de requests
import requests

response = requests.get('https://api.github.com')
print(response.status_code)

# Analyse de données avec pandas
import pandas as pd

data = {'Nom': ['Alice', 'Bob', 'Charlie'], 'Age': [25, 30, 35]}
df = pd.DataFrame(data)
print(df)

# Création de graphiques avec matplotlib
import matplotlib.pyplot as plt

x = range(11)
y = [i**2 for i in x]

plt.plot(x, y)
plt.xlabel('x')
plt.ylabel('y = x^2')
plt.title('Graphique de y = x^2')
plt.show()
```

#### Exercice 4 : Projet pratique - Jeu de devinettes

**Objectif** : Créer un jeu simple où l'utilisateur doit deviner un nombre entre 1 et 100.

1. **Jeu de devinettes**
   - Utilisez le module `random` pour générer un nombre aléatoire entre 1 et 100.
   - Demandez à l'utilisateur de deviner le nombre.
   - Indiquez si la devinette est trop haute ou trop basse.
   - Continuez jusqu'à ce que l'utilisateur trouve le nombre correct.

```python
import random

nombre_a_deviner = random.randint(1, 100)
deviner = False

print("Devinez le nombre entre 1 et 100")

while not deviner:
    try:
        choix = int(input("Votre devinette: "))
        if choix < nombre_a_deviner:
            print("Trop bas!")
        elif choix > nombre_a_deviner:
            print("Trop haut!")
        else:
            print("Félicitations! Vous avez deviné le nombre.")
            deviner = True
    except ValueError:
        print("Veuillez entrer un nombre valide.")
```

#### Exercice 5 : Projet pratique - Mini calculatrice

**Objectif** : Créer une mini calculatrice qui peut effectuer des opérations de base comme l'addition, la soustraction, la multiplication et la division.

1. **Mini calculatrice**
   - Demandez à l'utilisateur de choisir une opération: `+`, `-`, `*`, `/`.
   - Demandez à l'utilisateur d'entrer deux nombres.
   - Effectuez l'opération choisie et affichez le résultat.

```python
def addition(a, b):
    return a + b

def soustraction(a, b):
    return a - b

def multiplication(a, b):
    return a * b

def division(a, b):
    if b == 0:
        return "Erreur : Division par zéro!"
    return a / b

print("Sélectionnez l'opération :")
print("1. Addition")
print("2. Soustraction")
print("3. Multiplication")
print("4. Division")

choix = input("Entrez votre choix (1/2/3/4): ")

num1 = float(input("Entrez le premier nombre: "))
num2 = float(input("Entrez le second nombre: "))

if choix == '1':
    print(f"Résultat: {addition(num1, num2)}")
elif choix == '2':
    print(f"Résultat: {soustraction(num1, num2)}")
elif choix == '3':
    print(f"Résultat: {multiplication(num1, num2)}")
elif choix == '4':
    print(f"Résultat: {division(num1, num2)}")
else:
    print("Choix invalide")
```
### Exercices : Modules, Bibliothèques et Projets (Suite)

Ces exercices supplémentaires permettront aux débutants de renforcer leurs compétences en utilisant des modules, des bibliothèques externes et en travaillant sur des projets simples en Python.

#### Exercice 6 : Utilisation avancée de modules intégrés

**Objectif** : Approfondir l'utilisation des modules intégrés de Python comme `random`, `time`, et `statistics`.

1. **Génération de nombres aléatoires avec `random`**
   - Utilisez `random.randint` pour générer une liste de 10 nombres aléatoires entre 1 et 100.
   - Trouvez et affichez le nombre le plus élevé et le plus bas de la liste.

2. **Mesure du temps avec `time`**
   - Utilisez `time.time` pour mesurer le temps d'exécution d'une boucle `for` qui s'exécute 10 000 fois.
   - Affichez le temps d'exécution.

3. **Statistiques simples avec `statistics`**
   - Utilisez le module `statistics` pour calculer la moyenne, la médiane et l'écart type d'une liste de nombres.

```python
import random
import time
import statistics

# random
nombres_aleatoires = [random.randint(1, 100) for _ in range(10)]
print("Nombres aléatoires :", nombres_aleatoires)
print("Nombre le plus élevé :", max(nombres_aleatoires))
print("Nombre le plus bas :", min(nombres_aleatoires))

# time
debut = time.time()
for _ in range(10000):
    pass
fin = time.time()
print("Temps d'exécution :", fin - debut, "secondes")

# statistics
data = [10, 20, 20, 40, 50]
print("Moyenne :", statistics.mean(data))
print("Médiane :", statistics.median(data))
print("Écart type :", statistics.stdev(data))
```

#### Exercice 7 : Utilisation de bibliothèques externes

**Objectif** : Approfondir l'utilisation de bibliothèques externes en Python.

1. **Manipulation d'images avec `Pillow`**
   - Utilisez `pip` pour installer la bibliothèque `Pillow`.
   - Importez `PIL.Image`.
   - Ouvrez une image, affichez-la et sauvegardez une version réduite.

2. **Scraping de sites web avec `BeautifulSoup`**
   - Utilisez `pip` pour installer `requests` et `beautifulsoup4`.
   - Récupérez le contenu HTML d'une page web.
   - Utilisez `BeautifulSoup` pour extraire tous les titres (balises `<h1>`).

```python
# Manipulation d'images avec Pillow
from PIL import Image

image = Image.open('chemin/vers/votre/image.jpg')
image.show()
image_reduite = image.resize((100, 100))
image_reduite.save('chemin/vers/votre/image_reduite.jpg')

# Scraping avec BeautifulSoup
import requests
from bs4 import BeautifulSoup

url = 'https://www.example.com'
response = requests.get(url)
soup = BeautifulSoup(response.text, 'html.parser')

titres = soup.find_all('h1')
for titre in titres:
    print(titre.get_text())
```

#### Exercice 8 : Projet pratique - Analyse de texte

**Objectif** : Créer un script pour analyser un texte, comptabiliser le nombre de mots, de phrases, et de caractères.

1. **Analyse de texte**
   - Demandez à l'utilisateur de fournir un texte.
   - Comptez et affichez le nombre de mots, de phrases, et de caractères dans le texte.

```python
texte = input("Entrez un texte : ")

nombre_mots = len(texte.split())
nombre_phrases = texte.count('.') + texte.count('!') + texte.count('?')
nombre_caracteres = len(texte)

print("Nombre de mots :", nombre_mots)
print("Nombre de phrases :", nombre_phrases)
print("Nombre de caractères :", nombre_caracteres)
```

#### Exercice 9 : Projet pratique - Gestion de contacts

**Objectif** : Créer une application simple pour gérer des contacts.

1. **Gestion de contacts**
   - Demandez à l'utilisateur d'ajouter, de visualiser ou de supprimer des contacts.
   - Utilisez un dictionnaire pour stocker les contacts (nom, numéro de téléphone).

```python
contacts = {}

def ajouter_contact(nom, numero):
    contacts[nom] = numero
    print(f"Contact {nom} ajouté avec succès.")

def afficher_contacts():
    if contacts:
        for nom, numero in contacts.items():
            print(f"Nom: {nom}, Numéro: {numero}")
    else:
        print("Aucun contact trouvé.")

def supprimer_contact(nom):
    if nom in contacts:
        del contacts[nom]
        print(f"Contact {nom} supprimé avec succès.")
    else:
        print(f"Contact {nom} introuvable.")

while True:
    print("\n1. Ajouter un contact")
    print("2. Afficher les contacts")
    print("3. Supprimer un contact")
    print("4. Quitter")
    choix = input("Entrez votre choix : ")
    
    if choix == '1':
        nom = input("Entrez le nom : ")
        numero = input("Entrez le numéro : ")
        ajouter_contact(nom, numero)
    elif choix == '2':
        afficher_contacts()
    elif choix == '3':
        nom = input("Entrez le nom du contact à supprimer : ")
        supprimer_contact(nom)
    elif choix == '4':
        break
    else:
        print("Choix invalide. Veuillez réessayer.")
```

#### Exercice 10 : Projet pratique - Calculatrice scientifique

**Objectif** : Créer une calculatrice scientifique qui peut effectuer des opérations avancées comme le calcul des logarithmes, des exponentielles, et des fonctions trigonométriques.

```python
import math

def addition(a, b):
    return a + b

def soustraction(a, b):
    return a - b

def multiplication(a, b):
    return a * b

def division(a, b):
    if b == 0:
        return "Erreur : Division par zéro!"
    return a / b

def logarithme(a):
    return math.log(a)

def exponentielle(a):
    return math.exp(a)

def sinus(a):
    return math.sin(a)

print("Sélectionnez l'opération :")
print("1. Addition")
print("2. Soustraction")
print("3. Multiplication")
print("4. Division")
print("5. Logarithme")
print("6. Exponentielle")
print("7. Sinus")

choix = input("Entrez votre choix (1/2/3/4/5/6/7): ")

if choix in ['1', '2', '3', '4']:
    num1 = float(input("Entrez le premier nombre: "))
    num2 = float(input("Entrez le second nombre: "))
    if choix == '1':
        print(f"Résultat: {addition(num1, num2)}")
    elif choix == '2':
        print(f"Résultat: {soustraction(num1, num2)}")
    elif choix == '3':
        print(f"Résultat: {multiplication(num1, num2)}")
    elif choix == '4':
        print(f"Résultat: {division(num1, num2)}")
elif choix in ['5', '6', '7']:
    num = float(input("Entrez le nombre: "))
    if choix == '5':
        print(f"Résultat: {logarithme(num)}")
    elif choix == '6':
        print(f"Résultat: {exponentielle(num)}")
    elif choix == '7':
        print(f"Résultat: {sinus(num)}")
else:
    print("Choix invalide")
```
