# Exercices
Voici quelques exercices de niveau débutant axés sur les opérations arithmétiques en Python. Ces exercices aideront les apprenants à se familiariser avec les concepts de base des opérations arithmétiques telles que l'addition, la soustraction, la multiplication, la division et les opérateurs modulo.

Ces exercices permettent aux débutants de pratiquer les opérations arithmétiques de base tout en renforçant leur compréhension de la saisie et de la manipulation des données en Python.

## Exercice 1 : Addition de deux nombres
Écrivez un programme qui demande à l'utilisateur de saisir deux nombres, puis affiche la somme de ces deux nombres.

```python
# Demander à l'utilisateur de saisir deux nombres
nombre1 = float(input("Entrez le premier nombre : "))
nombre2 = float(input("Entrez le deuxième nombre : "))

# Calculer la somme
somme = nombre1 + nombre2

# Afficher le résultat
print(f"La somme de {nombre1} et {nombre2} est {somme}.")
```

## Exercice 2 : Calcul du reste
Écrivez un programme qui demande à l'utilisateur de saisir deux nombres entiers, puis affiche le reste de la division du premier nombre par le deuxième.

```python
# Demander à l'utilisateur de saisir deux nombres entiers
nombre1 = int(input("Entrez le premier nombre entier : "))
nombre2 = int(input("Entrez le deuxième nombre entier : "))

# Calculer le reste
reste = nombre1 % nombre2

# Afficher le résultat
print(f"Le reste de la division de {nombre1} par {nombre2} est {reste}.")
```

## Exercice 3 : Calcul de la puissance
Écrivez un programme qui demande à l'utilisateur de saisir deux nombres, puis affiche le premier nombre élevé à la puissance du deuxième nombre.

```python
# Demander à l'utilisateur de saisir deux nombres
base = float(input("Entrez la base : "))
exposant = float(input("Entrez l'exposant : "))

# Calculer la puissance
puissance = base ** exposant

# Afficher le résultat
print(f"{base} élevé à la puissance de {exposant} est {puissance}.")
```

## Exercice 4 : Moyenne de trois nombres
Écrivez un programme qui demande à l'utilisateur de saisir trois nombres, puis calcule et affiche leur moyenne.

```python
# Demander à l'utilisateur de saisir trois nombres
nombre1 = float(input("Entrez le premier nombre : "))
nombre2 = float(input("Entrez le deuxième nombre : "))
nombre3 = float(input("Entrez le troisième nombre : "))

# Calculer la moyenne
moyenne = (nombre1 + nombre2 + nombre3) / 3

# Afficher le résultat
print(f"La moyenne de {nombre1}, {nombre2} et {nombre3} est {moyenne}.")
```
## Exercice 5 : Calculatrice de base
Écrivez un programme qui demande à l'utilisateur de saisir deux nombres et un opérateur (+, -, *, /). Le programme doit effectuer l'opération et afficher le résultat.

```python
# Demander à l'utilisateur de saisir deux nombres et un opérateur
nombre1 = float(input("Entrez le premier nombre : "))
operateur = input("Entrez un opérateur (+, -, *, /) : ")
nombre2 = float(input("Entrez le deuxième nombre : "))

# Effectuer l'opération et afficher le résultat
if operateur == '+':
    resultat = nombre1 + nombre2
elif operateur == '-':
    resultat = nombre1 - nombre2
elif operateur == '*':
    resultat = nombre1 * nombre2
elif operateur == '/':
    resultat = nombre1 / nombre2
else:
    print("Opérateur invalide")

print(f"Le résultat de {nombre1} {operateur} {nombre2} est {resultat}.")
```
