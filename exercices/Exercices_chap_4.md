# Exercices Débutants : Programmation Orientée Objet (POO)

Ces exercices visent à introduire les concepts de base de la programmation orientée objet en Python pour les débutants.
Ces exercices couvrent les concepts de base de la programmation orientée objet en Python, y compris les classes, l'héritage, l'encapsulation et le polymorphisme. Ils sont conçus pour aider les débutants à comprendre et à appliquer ces concepts fondamentaux.

## Exercice 1 : Création d'une classe `Personne`

**Objectif** : Créer une classe représentant une personne avec des attributs et des méthodes de base.

**Instructions** :
- Définissez une classe `Personne` avec les attributs `nom` et `âge`.
- Ajoutez une méthode `se_presenter` qui affiche "Bonjour, je m'appelle [nom] et j'ai [âge] ans."

```python
class Personne:
    def __init__(self, nom, age):
        self.nom = nom
        self.age = age

    def se_presenter(self):
        print(f"Bonjour, je m'appelle {self.nom} et j'ai {self.age} ans.")

# Exemple d'utilisation
personne1 = Personne("Alice", 30)
personne1.se_presenter()
```

## Exercice 2 : Création d'une classe `Voiture`

**Objectif** : Créer une classe représentant une voiture avec des attributs et des méthodes.

**Instructions** :
- Définissez une classe `Voiture` avec les attributs `marque`, `modele` et `année`.
- Ajoutez une méthode `afficher_details` qui affiche "Voiture : [marque] [modele], Année : [année]."

```python
class Voiture:
    def __init__(self, marque, modele, annee):
        self.marque = marque
        self.modele = modele
        self.annee = annee

    def afficher_details(self):
        print(f"Voiture : {self.marque} {self.modele}, Année : {self.annee}")

# Exemple d'utilisation
voiture1 = Voiture("Toyota", "Corolla", 2020)
voiture1.afficher_details()
```

## Exercice 3 : Héritage

**Objectif** : Créer une classe `Etudiant` qui hérite de la classe `Personne`.

**Instructions** :
- Définissez une classe `Etudiant` qui hérite de `Personne`.
- Ajoutez un attribut `etudes` pour stocker le domaine d'études de l'étudiant.
- Ajoutez une méthode `se_presenter` qui affiche "Bonjour, je m'appelle [nom] et j'étudie [etudes]."

```python
class Personne:
    def __init__(self, nom, age):
        self.nom = nom
        self.age = age

    def se_presenter(self):
        print(f"Bonjour, je m'appelle {self.nom} et j'ai {self.age} ans.")

class Etudiant(Personne):
    def __init__(self, nom, age, etudes):
        super().__init__(nom, age)
        self.etudes = etudes

    def se_presenter(self):
        print(f"Bonjour, je m'appelle {self.nom} et j'étudie {self.etudes}.")

# Exemple d'utilisation
etudiant1 = Etudiant("Bob", 20, "Informatique")
etudiant1.se_presenter()
```

## Exercice 4 : Encapsulation

**Objectif** : Implémenter l'encapsulation dans une classe `CompteBancaire`.

**Instructions** :
- Définissez une classe `CompteBancaire` avec un attribut privé `_solde`.
- Ajoutez des méthodes `depot` et `retrait` pour modifier le solde.
- Ajoutez une méthode `afficher_solde` pour afficher le solde actuel.

```python
class CompteBancaire:
    def __init__(self, solde_initial):
        self._solde = solde_initial

    def depot(self, montant):
        self._solde += montant

    def retrait(self, montant):
        if montant <= self._solde:
            self._solde -= montant
        else:
            print("Solde insuffisant.")

    def afficher_solde(self):
        print(f"Solde actuel : {self._solde} €")

# Exemple d'utilisation
compte = CompteBancaire(100)
compte.depot(50)
compte.retrait(30)
compte.afficher_solde()
```

## Exercice 5 : Polymorphisme

**Objectif** : Implémenter le polymorphisme avec des méthodes redéfinies.

**Instructions** :
- Créez une classe `Animal` avec une méthode `parler`.
- Créez des classes `Chien` et `Chat` qui héritent de `Animal` et redéfinissent la méthode `parler` pour afficher respectivement "Woof" et "Meow".

```python
class Animal:
    def parler(self):
        pass

class Chien(Animal):
    def parler(self):
        print("Woof")

class Chat(Animal):
    def parler(self):
        print("Meow")

# Exemple d'utilisation
animaux = [Chien(), Chat()]

for animal in animaux:
    animal.parler()
```

## Exercice 6 : Gestion d'inventaire

**Objectif** : Créer une classe `Produit` et une classe `Inventaire` pour gérer une liste de produits.

**Instructions** :
- Définissez une classe `Produit` avec des attributs `nom`, `quantite` et `prix`.
- Définissez une classe `Inventaire` qui contient une liste de produits.
- Ajoutez des méthodes à `Inventaire` pour ajouter un produit, supprimer un produit par nom, et afficher tous les produits avec leurs détails.

```python
class Produit:
    def __init__(self, nom, quantite, prix):
        self.nom = nom
        self.quantite = quantite
        self.prix = prix

    def afficher_details(self):
        print(f"Produit: {self.nom}, Quantité: {self.quantite}, Prix: {self.prix} €")

class Inventaire:
    def __init__(self):
        self.produits = []

    def ajouter_produit(self, produit):
        self.produits.append(produit)

    def supprimer_produit(self, nom):
        self.produits = [produit for produit in self.produits if produit.nom != nom]

    def afficher_inventaire(self):
        for produit in self.produits:
            produit.afficher_details()

# Exemple d'utilisation
p1 = Produit("Pomme", 10, 0.5)
p2 = Produit("Banane", 20, 0.3)

inventaire = Inventaire()
inventaire.ajouter_produit(p1)
inventaire.ajouter_produit(p2)
inventaire.afficher_inventaire()
```

## Exercice 7 : Système de bibliothèque

**Objectif** : Créer des classes pour gérer un système de bibliothèque.

**Instructions** :
- Définissez une classe `Livre` avec des attributs `titre`, `auteur` et `disponible`.
- Définissez une classe `Bibliotheque` avec une liste de livres.
- Ajoutez des méthodes à `Bibliotheque` pour ajouter un livre, emprunter un livre, retourner un livre, et afficher tous les livres avec leur état de disponibilité.

```python
class Livre:
    def __init__(self, titre, auteur):
        self.titre = titre
        self.auteur = auteur
        self.disponible = True

    def afficher_details(self):
        statut = "Disponible" if self.disponible else "Emprunté"
        print(f"Livre: {self.titre} par {self.auteur}, Statut: {statut}")

class Bibliotheque:
    def __init__(self):
        self.livres = []

    def ajouter_livre(self, livre):
        self.livres.append(livre)

    def emprunter_livre(self, titre):
        for livre in self.livres:
            if livre.titre == titre and livre.disponible:
                livre.disponible = False
                return f"Vous avez emprunté {titre}."
        return f"Le livre {titre} n'est pas disponible."

    def retourner_livre(self, titre):
        for livre in self.livres:
            if livre.titre == titre and not livre.disponible:
                livre.disponible = True
                return f"Vous avez retourné {titre}."
        return f"Le livre {titre} n'était pas emprunté."

    def afficher_livres(self):
        for livre in self.livres:
            livre.afficher_details()

# Exemple d'utilisation
l1 = Livre("1984", "George Orwell")
l2 = Livre("Le Petit Prince", "Antoine de Saint-Exupéry")

bibliotheque = Bibliotheque()
bibliotheque.ajouter_livre(l1)
bibliotheque.ajouter_livre(l2)
bibliotheque.afficher_livres()
print(bibliotheque.emprunter_livre("1984"))
bibliotheque.afficher_livres()
print(bibliotheque.retourner_livre("1984"))
bibliotheque.afficher_livres()
```

## Exercice 8 : Polymorphisme avec des formes géométriques

**Objectif** : Utiliser le polymorphisme pour travailler avec des formes géométriques.

**Instructions** :
- Définissez une classe de base `Forme` avec une méthode `aire`.
- Créez des classes `Rectangle` et `Cercle` qui héritent de `Forme` et implémentent la méthode `aire`.
- Utilisez une liste pour stocker des instances de `Rectangle` et `Cercle` et appelez la méthode `aire` pour chaque forme.

```python
import math

class Forme:
    def aire(self):
        pass

class Rectangle(Forme):
    def __init__(self, largeur, hauteur):
        self.largeur = largeur
        self.hauteur = hauteur

    def aire(self):
        return self.largeur * self.hauteur

class Cercle(Forme):
    def __init__(self, rayon):
        self.rayon = rayon

    def aire(self):
        return math.pi * (self.rayon ** 2)

# Exemple d'utilisation
formes = [Rectangle(3, 4), Cercle(5)]

for forme in formes:
    print(f"Aire de la forme: {forme.aire()}")
```

## Exercice 9 : Système de facturation

**Objectif** : Créer des classes pour gérer un système de facturation.

**Instructions** :
- Définissez une classe `Produit` avec des attributs `nom`, `quantite` et `prix_unitaire`.
- Définissez une classe `Facture` avec une liste de produits.
- Ajoutez des méthodes à `Facture` pour ajouter un produit, supprimer un produit par nom, calculer le total et afficher tous les produits avec leur prix total.

```python
class Produit:
    def __init__(self, nom, quantite, prix_unitaire):
        self.nom = nom
        self.quantite = quantite
        self.prix_unitaire = prix_unitaire

    def prix_total(self):
        return self.quantite * self.prix_unitaire

    def afficher_details(self):
        print(f"Produit: {self.nom}, Quantité: {self.quantite}, Prix Unitaire: {self.prix_unitaire} €, Prix Total: {self.prix_total()} €")

class Facture:
    def __init__(self):
        self.produits = []

    def ajouter_produit(self, produit):
        self.produits.append(produit)

    def supprimer_produit(self, nom):
        self.produits = [produit for produit in self.produits if produit.nom != nom]

    def calculer_total(self):
        return sum(produit.prix_total() for produit in self.produits)

    def afficher_facture(self):
        for produit in self.produits:
            produit.afficher_details()
        print(f"Total de la facture: {self.calculer_total()} €")

# Exemple d'utilisation
p1 = Produit("Pomme", 10, 0.5)
p2 = Produit("Banane", 5, 0.3)

facture = Facture()
facture.ajouter_produit(p1)
facture.ajouter_produit(p2)
facture.afficher_facture()
```

## Exercice 10 : Gestion des utilisateurs

**Objectif** : Créer des classes pour gérer des utilisateurs et des administrateurs avec des privilèges différents.

**Instructions** :
- Définissez une classe `Utilisateur` avec des attributs `nom` et `email`.
- Créez une classe `Administrateur` qui hérite de `Utilisateur` et ajoute un attribut `privileges`.
- Ajoutez des méthodes à `Administrateur` pour ajouter et supprimer des privilèges.
- Ajoutez une méthode à `Utilisateur` pour afficher les détails de l'utilisateur.

```python
class Utilisateur:
    def __init__(self, nom, email):
        self.nom = nom
        self.email = email

    def afficher_details(self):
        print(f"Utilisateur: {self.nom}, Email: {self.email}")

class Administrateur(Utilisateur):
    def __init__(self, nom, email):
        super().__init__(nom, email)
        self.privileges = []

    def ajouter_privilege(self, privilege):
        self.privileges.append(privilege)

    def supprimer_privilege(self, privilege):
        self.privileges.remove(privilege)

    def afficher_privileges(self):
        print(f"Privilèges de {self.nom}: {', '.join(self.privileges)}")

# Exemple d'utilisation
admin = Administrateur("Alice", "alice@example.com")
admin.ajouter_privilege("Peut supprimer des utilisateurs")
admin.ajouter_privilege("Peut ajouter des utilisateurs")
admin.afficher_details()
admin.afficher_privileges()
```
