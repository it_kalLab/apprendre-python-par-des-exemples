# Exercices pour débutants : Structures de données et manipulation

Ces exercices aideront les débutants à se familiariser avec les principales structures de données en Python (listes, tuples, ensembles et dictionnaires) ainsi que les techniques de manipulation.
Ces exercices fourniront aux débutants une pratique concrète et pratique des concepts fondamentaux des structures de données et de leur manipulation en Python.

## Exercice 1 : Manipulation de Listes
1. **Création et modification de liste**
   - Créez une liste nommée `fruits` contenant les éléments suivants : "pomme", "banane", "cerise".
   - Ajoutez "orange" à la fin de la liste.
   - Remplacez "banane" par "mangue".
   - Supprimez "cerise" de la liste.

   ```python
   fruits = ["pomme", "banane", "cerise"]
   fruits.append("orange")
   fruits[1] = "mangue"
   fruits.remove("cerise")
   print(fruits)  # Affiche : ['pomme', 'mangue', 'orange']
   ```

2. **Listes imbriquées et compréhension de liste**
   - Créez une liste imbriquée nommée `matrix` avec les sous-listes suivantes : [1, 2, 3], [4, 5, 6], [7, 8, 9].
   - Utilisez la compréhension de liste pour créer une nouvelle liste `squares` contenant les carrés des nombres de 1 à 10.

   ```python
   matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
   squares = [x**2 for x in range(1, 11)]
   print(squares)  # Affiche : [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
   ```

## Exercice 2 : Tuples et Ensembles
1. **Manipulation de Tuples**
   - Créez un tuple nommé `colors` contenant les éléments "rouge", "vert", "bleu".
   - Accédez et affichez le deuxième élément du tuple.

   ```python
   colors = ("rouge", "vert", "bleu")
   print(colors[1])  # Affiche : vert
   ```

2. **Manipulation d'Ensembles**
   - Créez un ensemble nommé `numbers` contenant les éléments 1, 2, 3, 4, 5.
   - Ajoutez 6 à l'ensemble.
   - Supprimez 3 de l'ensemble.
   - Affichez l'union de `numbers` avec un autre ensemble `{4, 5, 6, 7, 8}`.

   ```python
   numbers = {1, 2, 3, 4, 5}
   numbers.add(6)
   numbers.remove(3)
   other_set = {4, 5, 6, 7, 8}
   union_set = numbers | other_set
   print(union_set)  # Affiche : {1, 2, 4, 5, 6, 7, 8}
   ```

## Exercice 3 : Manipulation de Dictionnaires
1. **Création et manipulation de dictionnaires**
   - Créez un dictionnaire nommé `student` avec les paires clé-valeur suivantes : "nom": "Alice", "age": 21, "note": 90.
   - Ajoutez une nouvelle paire clé-valeur "ville": "Paris".
   - Modifiez la valeur de la clé "note" à 95.
   - Supprimez la paire clé-valeur "age".

   ```python
   student = {"nom": "Alice", "age": 21, "note": 90}
   student["ville"] = "Paris"
   student["note"] = 95
   del student["age"]
   print(student)  # Affiche : {'nom': 'Alice', 'note': 95, 'ville': 'Paris'}
   ```

## Exercice 4 : Structures de contrôle
1. **Conditions**
   - Écrivez un programme qui demande à l'utilisateur de saisir un nombre et affiche si ce nombre est positif, négatif ou zéro.

   ```python
   nombre = float(input("Entrez un nombre : "))
   if nombre > 0:
       print("Le nombre est positif.")
   elif nombre == 0:
       print("Le nombre est zéro.")
   else:
       print("Le nombre est négatif.")
   ```

2. **Boucles**
   - Écrivez un programme qui affiche tous les nombres pairs de 1 à 20 en utilisant une boucle `for`.

   ```python
   for i in range(1, 21):
       if i % 2 == 0:
           print(i)
   ```

## Exercice 5 : Manipulation de fichiers
1. **Lecture et écriture de fichiers**
   - Écrivez un programme qui crée un fichier nommé `message.txt`, écrit "Bonjour, monde!" dedans, puis lit et affiche le contenu du fichier.

   ```python
   with open("message.txt", "w") as fichier:
       fichier.write("Bonjour, monde!")
   
   with open("message.txt", "r") as fichier:
       contenu = fichier.read()
       print(contenu)  # Affiche : Bonjour, monde!
   ```

## Exercice 6 : Fonctions
1. **Définir et appeler des fonctions**
   - Écrivez une fonction nommée `multiplication` qui prend deux paramètres et retourne leur produit. Appelez la fonction avec les arguments 3 et 4 et affichez le résultat.

   ```python
   def multiplication(a, b):
       return a * b

   resultat = multiplication(3, 4)
   print(resultat)  # Affiche : 12
   ```

2. **Paramètres et valeurs de retour**
   - Écrivez une fonction nommée `saluer` qui prend un paramètre `nom` et affiche "Bonjour, `nom`!". Appelez la fonction avec l'argument "Alice".

   ```python
   def saluer(nom):
       print(f"Bonjour, {nom}!")

   saluer("Alice")  # Affiche : Bonjour, Alice!
   ```

