Voici une série d'exercices adaptés pour un **Chapitre 6 : Gestion des Fichiers**, dans le cadre de l'apprentissage de la manipulation de fichiers en Python.

### Exercice 1 : Lecture d'un fichier texte

**Objectif** : Savoir ouvrir et lire le contenu d'un fichier texte.

- Créez un fichier texte nommé `exemple.txt` qui contient plusieurs lignes de texte.
- Écrivez un script Python qui ouvre ce fichier et affiche son contenu ligne par ligne dans la console.

```python
# Lecture d'un fichier texte
with open('exemple.txt', 'r') as fichier:
    for ligne in fichier:
        print(ligne.strip())  # .strip() pour enlever les sauts de ligne
```

### Exercice 2 : Écriture dans un fichier texte

**Objectif** : Apprendre à écrire du contenu dans un fichier texte.

- Créez un script qui demande à l'utilisateur de saisir son nom et son âge.
- Écrivez ces informations dans un fichier texte appelé `informations.txt` sous la forme : `Nom: <nom>, Âge: <âge>`.

```python
nom = input("Entrez votre nom : ")
age = input("Entrez votre âge : ")

with open('informations.txt', 'w') as fichier:
    fichier.write(f"Nom: {nom}, Âge: {age}")
```

### Exercice 3 : Comptage de mots dans un fichier

**Objectif** : Compter le nombre de mots dans un fichier texte.

- Utilisez un fichier texte nommé `document.txt` contenant plusieurs paragraphes.
- Écrivez un script Python qui ouvre ce fichier et compte combien de mots il contient au total.

```python
with open('document.txt', 'r') as fichier:
    contenu = fichier.read()
    mots = contenu.split()
    print(f"Le fichier contient {len(mots)} mots.")
```

### Exercice 4 : Copie de fichier

**Objectif** : Apprendre à copier le contenu d'un fichier dans un autre fichier.

- Créez un fichier `source.txt` avec du contenu.
- Écrivez un script qui copie le contenu de `source.txt` dans un autre fichier appelé `destination.txt`.

```python
with open('source.txt', 'r') as source:
    contenu = source.read()

with open('destination.txt', 'w') as destination:
    destination.write(contenu)
```

### Exercice 5 : Recherche d'un mot spécifique dans un fichier

**Objectif** : Apprendre à rechercher un mot dans un fichier texte.

- Créez un fichier texte `journal.txt` contenant plusieurs lignes.
- Écrivez un script qui demande à l'utilisateur de saisir un mot, puis recherche ce mot dans le fichier.
- Affichez un message indiquant si le mot a été trouvé et sur quelle ligne.

```python
mot_a_rechercher = input("Entrez le mot à rechercher : ")

with open('journal.txt', 'r') as fichier:
    lignes = fichier.readlines()
    for i, ligne in enumerate(lignes, start=1):
        if mot_a_rechercher in ligne:
            print(f"Le mot '{mot_a_rechercher}' a été trouvé à la ligne {i}.")
```

### Exercice 6 : Manipulation de fichiers CSV

**Objectif** : Manipuler des fichiers CSV avec Python.

- Créez un fichier `donnees.csv` qui contient des données sous forme de tableau (par exemple, nom, âge, ville).
- Écrivez un script Python qui lit ce fichier CSV et affiche chaque ligne sous forme de dictionnaire.

```python
import csv

with open('donnees.csv', 'r') as fichier_csv:
    lecteur_csv = csv.DictReader(fichier_csv)
    for ligne in lecteur_csv:
        print(ligne)
```

### Exercice 7 : Ajout de données dans un fichier

**Objectif** : Apprendre à ajouter des données à la fin d'un fichier existant sans le remplacer.

- Écrivez un script qui demande à l'utilisateur d'ajouter une nouvelle entrée (nom et âge) dans un fichier texte `informations.txt` sans écraser les informations précédentes.

```python
nom = input("Entrez votre nom : ")
age = input("Entrez votre âge : ")

with open('informations.txt', 'a') as fichier:
    fichier.write(f"\nNom: {nom}, Âge: {age}")
```

### Exercice 8 : Suppression de lignes spécifiques dans un fichier

**Objectif** : Savoir supprimer une ou plusieurs lignes spécifiques d'un fichier texte.

- Créez un fichier texte `notes.txt` contenant plusieurs lignes.
- Écrivez un script Python qui lit le fichier, supprime une ligne spécifique (par exemple, la ligne 2), puis enregistre les modifications dans un nouveau fichier `notes_modifiees.txt`.

```python
with open('notes.txt', 'r') as fichier:
    lignes = fichier.readlines()

# Suppression de la deuxième ligne
del lignes[1]  # 0 correspond à la première ligne

with open('notes_modifiees.txt', 'w') as fichier_modifie:
    fichier_modifie.writelines(lignes)
```

### Exercice 9 : Gestion des erreurs lors de la manipulation de fichiers

**Objectif** : Apprendre à gérer les erreurs lors de la lecture/écriture de fichiers.

- Écrivez un script qui tente de lire un fichier qui n'existe pas, puis qui gère cette erreur avec un `try-except`.

```python
try:
    with open('fichier_inexistant.txt', 'r') as fichier:
        contenu = fichier.read()
except FileNotFoundError:
    print("Le fichier n'existe pas.")
```

### Exercice 10 : Organisation des données dans un fichier

**Objectif** : Créer et structurer un fichier texte pour organiser des informations.

- Écrivez un script Python qui crée un fichier `contacts.txt` et permet à l'utilisateur d'ajouter plusieurs contacts (nom, prénom, téléphone). Affichez tous les contacts enregistrés lorsque l'utilisateur a fini d'ajouter des contacts.

```python
def ajouter_contact():
    nom = input("Entrez le nom : ")
    prenom = input("Entrez le prénom : ")
    telephone = input("Entrez le numéro de téléphone : ")
    with open('contacts.txt', 'a') as fichier:
        fichier.write(f"{nom}, {prenom}, {telephone}\n")

while True:
    ajouter_contact()
    continuer = input("Voulez-vous ajouter un autre contact ? (o/n) : ")
    if continuer.lower() != 'o':
        break

# Afficher les contacts enregistrés
print("\nListe des contacts :")
with open('contacts.txt', 'r') as fichier:
    print(fichier.read())
```