### Exercices pour débutants : Contrôle de Flux

Ces exercices aideront les débutants à comprendre et à pratiquer les concepts de contrôle de flux en Python, incluant les structures conditionnelles et les boucles.
Ces exercices couvrent les bases des structures de contrôle en Python et fournissent une bonne pratique pour les débutants.

#### Exercice 1 : Conditions
1. **Vérification de nombre**
   - Écrivez un programme qui demande à l'utilisateur de saisir un nombre et affiche s'il est positif, négatif ou zéro.

   ```python
   nombre = float(input("Entrez un nombre : "))
   if nombre > 0:
       print("Le nombre est positif.")
   elif nombre == 0:
       print("Le nombre est zéro.")
   else:
       print("Le nombre est négatif.")
   ```

2. **Évaluation des notes**
   - Écrivez un programme qui demande à l'utilisateur de saisir une note sur 20 et affiche la mention correspondante :
     - "Très bien" pour une note entre 16 et 20.
     - "Bien" pour une note entre 12 et 15.
     - "Assez bien" pour une note entre 8 et 11.
     - "Insuffisant" pour une note inférieure à 8.

   ```python
   note = float(input("Entrez une note sur 20 : "))
   if note >= 16:
       print("Très bien")
   elif note >= 12:
       print("Bien")
   elif note >= 8:
       print("Assez bien")
   else:
       print("Insuffisant")
   ```

#### Exercice 2 : Boucles
1. **Affichage des nombres pairs**
   - Écrivez un programme qui affiche tous les nombres pairs de 1 à 20.

   ```python
   for i in range(1, 21):
       if i % 2 == 0:
           print(i)
   ```

2. **Somme des nombres**
   - Écrivez un programme qui calcule et affiche la somme des nombres de 1 à 100.

   ```python
   somme = 0
   for i in range(1, 101):
       somme += i
   print("La somme des nombres de 1 à 100 est :", somme)
   ```

3. **Mot de passe**
   - Écrivez un programme qui demande à l'utilisateur de saisir un mot de passe. Tant que le mot de passe n'est pas "python123", le programme doit continuer à demander le mot de passe.

   ```python
   mot_de_passe = ""
   while mot_de_passe != "python123":
       mot_de_passe = input("Entrez le mot de passe : ")
       if mot_de_passe == "python123":
           print("Accès autorisé")
       else:
           print("Mot de passe incorrect, essayez encore.")
   ```

#### Exercice 3 : Boucles imbriquées
1. **Table de multiplication**
   - Écrivez un programme qui affiche la table de multiplication de 1 à 10.

   ```python
   for i in range(1, 11):
       for j in range(1, 11):
           print(f"{i} x {j} = {i * j}")
       print("-" * 15)
   ```

2. **Étoiles**
   - Écrivez un programme qui affiche un triangle d'étoiles comme suit :
     ```
     *
     **
     ***
     ****
     *****
     ```

   ```python
   for i in range(1, 6):
       print("*" * i)
   ```

#### Exercice 4 : Utilisation de `break` et `continue`
1. **Interruption de boucle**
   - Écrivez un programme qui affiche les nombres de 1 à 10, mais s'arrête si le nombre atteint 5.

   ```python
   for i in range(1, 11):
       if i == 5:
           break
       print(i)
   ```

2. **Passer les nombres pairs**
   - Écrivez un programme qui affiche les nombres de 1 à 10, mais saute les nombres pairs.

   ```python
   for i in range(1, 11):
       if i % 2 == 0:
           continue
       print(i)
   ```

#### Exercice 5 : Combinaison de conditions et de boucles
##### Trouver les nombres premiers

**Objectif** : Écrire un programme qui affiche tous les nombres premiers de 2 à 50.

**Instructions** : 
- Un nombre premier est un nombre qui est uniquement divisible par 1 et par lui-même.
- Utilisez une boucle pour parcourir les nombres de 2 à 50.
- Pour chaque nombre, utilisez une autre boucle pour vérifier s'il est divisible par un nombre autre que 1 et lui-même.
- Si vous trouvez un diviseur, le nombre n'est pas premier ; sinon, il l'est.

Voici un exemple de solution avec explications et commentaires :

```python
# Boucle sur chaque nombre de 2 à 50
for num in range(2, 51):
    # Supposons d'abord que le nombre est premier
    is_prime = True
    # Boucle de vérification des diviseurs potentiels
    for i in range(2, num):
        # Si le nombre est divisible par un autre nombre que 1 et lui-même
        if num % i == 0:
            # Alors ce n'est pas un nombre premier
            is_prime = False
            # Pas besoin de vérifier plus loin, on peut sortir de la boucle
            break
    # Si is_prime est toujours True, le nombre est premier
    if is_prime:
        print(num)
```

**Explication** :
1. **Initialisation de la boucle principale** : La boucle principale `for num in range(2, 51)` parcourt chaque nombre de 2 à 50.
2. **Supposition initiale** : La variable `is_prime` est initialisée à `True`, supposant que le nombre est premier jusqu'à preuve du contraire.
3. **Vérification des diviseurs** : La boucle interne `for i in range(2, num)` vérifie chaque potentiel diviseur du nombre `num`.
   - Si `num % i == 0`, cela signifie que `num` est divisible par `i`, donc `num` n'est pas un nombre premier.
   - Dans ce cas, `is_prime` est mis à `False` et la boucle interne se termine prématurément grâce à `break`.
4. **Affichage des nombres premiers** : Après la vérification, si `is_prime` est toujours `True`, `num` est un nombre premier et il est affiché.


#### Exercice 6 : Somme des nombres impairs
**Objectif** : Calculer et afficher la somme de tous les nombres impairs de 1 à 100.

**Instructions** :
- Utilisez une boucle pour parcourir les nombres de 1 à 100.
- Utilisez une condition pour vérifier si le nombre est impair.
- Si le nombre est impair, ajoutez-le à une variable de somme.

```python
somme = 0
for num in range(1, 101):
    if num % 2 != 0:
        somme += num
print("La somme des nombres impairs de 1 à 100 est :", somme)
```

#### Exercice 7 : Nombre parfait
**Objectif** : Vérifier si un nombre donné est un nombre parfait.

**Instructions** :
- Un nombre parfait est un nombre qui est égal à la somme de ses diviseurs propres (c'est-à-dire les diviseurs autres que lui-même).
- Demandez à l'utilisateur de saisir un nombre.
- Utilisez une boucle pour trouver les diviseurs du nombre et une condition pour vérifier s'ils sont des diviseurs propres.
- Calculez la somme des diviseurs propres et comparez-la au nombre initial.

```python
nombre = int(input("Entrez un nombre : "))
somme_diviseurs = 0
for i in range(1, nombre):
    if nombre % i == 0:
        somme_diviseurs += i

if somme_diviseurs == nombre:
    print(f"{nombre} est un nombre parfait.")
else:
    print(f"{nombre} n'est pas un nombre parfait.")
```

#### Exercice 8 : Nombre Armstrong
**Objectif** : Vérifier si un nombre à trois chiffres est un nombre Armstrong.

**Instructions** :
- Un nombre Armstrong de trois chiffres est un nombre tel que la somme des cubes de ses chiffres est égale au nombre lui-même.
- Demandez à l'utilisateur de saisir un nombre à trois chiffres.
- Séparez les chiffres du nombre et calculez la somme des cubes des chiffres.
- Comparez cette somme au nombre initial.

```python
nombre = int(input("Entrez un nombre à trois chiffres : "))
somme_cubes = 0
temp = nombre

while temp > 0:
    chiffre = temp % 10
    somme_cubes += chiffre ** 3
    temp //= 10

if somme_cubes == nombre:
    print(f"{nombre} est un nombre Armstrong.")
else:
    print(f"{nombre} n'est pas un nombre Armstrong.")
```

#### Exercice 9 : Séquence de Fibonacci
**Objectif** : Afficher la séquence de Fibonacci jusqu'à un nombre donné de termes.

**Instructions** :
- La séquence de Fibonacci commence par 0 et 1, et chaque terme suivant est la somme des deux précédents.
- Demandez à l'utilisateur de saisir le nombre de termes de la séquence à afficher.
- Utilisez une boucle pour générer et afficher les termes de la séquence.

```python
n = int(input("Entrez le nombre de termes de la séquence de Fibonacci : "))
a, b = 0, 1
count = 0

if n <= 0:
    print("Veuillez entrer un nombre positif.")
elif n == 1:
    print("Séquence de Fibonacci jusqu'à", n, "terme :")
    print(a)
else:
    print("Séquence de Fibonacci :")
    while count < n:
        print(a)
        nth = a + b
        a = b
        b = nth
        count += 1
```

#### Exercice 10 : Mot le plus long
**Objectif** : Trouver le mot le plus long dans une phrase donnée par l'utilisateur.

**Instructions** :
- Demandez à l'utilisateur de saisir une phrase.
- Séparez la phrase en mots.
- Utilisez une boucle pour trouver le mot le plus long.

```python
phrase = input("Entrez une phrase : ")
mots = phrase.split()
mot_plus_long = ""

for mot in mots:
    if len(mot) > len(mot_plus_long):
        mot_plus_long = mot

print("Le mot le plus long est :", mot_plus_long)
```

