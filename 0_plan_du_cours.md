# Plan du cours
[Chapitre 0 : Généralités et historiques](cours/Chapitre_0.md)

[Chapitre I : Prise en Main de Python](cours/Chapitre_1.md)

- [Exercices du chapitre 1](exercices/Exercices_chap_1.md)

[Chapitre II : Structures de données et manipulation](cours/Chapitre_2.md)

- [Exercices du chapitre 2](exercices/Exercices_chap_2.md)

[Chapitre III : Contrôle de Flux](cours/Chapitre_3.md)

- [Exercices du chapitre 3](exercices/Exercices_chap_3.md)

[Chapitre IV : Programmation orientée objet (POO)](cours/Chapitre_4.md)

- [Exercices du chapitre 4](exercices/Exercices_chap_4.md)

[Chapitre V : Modules, Bibliothèques et Projets](cours/Chapitre_5.md)

- [Exercices du chapitre 5](exercices/Exercices_chap_5.md)

[Chapitre VI : Gestion des Fichiers](cours/Chapitre_6.md)

- [Exercices du chapitre 6](exercices/Exercices_chap_6.md)

[Chapitre VII : Avancé et Pratique Continue](cours/Chapitre_7.md)

- [Exercices du chapitre 7](exercices/Exercices_chap_7.md)

[Chapitre VIII : Avancé](cours/Chapitre_8.md)

- [Exercices du chapitre 8](exercices/Exercices_chap_8.md)


[Chapitre IX : Projets Pratiques](cours/Chapitre_9.md)

- [Exercices du chapitre 9](exercices/Exercices_chap_9.md)


[Chapitre X : Ressources](cours/Chapitre_10.md)

