```python
import pandas as pd

etudiant_df = pd.read_csv("etudiants-1.csv")

#print(etudiant_df)

#print(etudiant_df.head(10))

nom_age_sex_moyenne = etudiant_df[["Nom", "Age", "Sexe", "Moyenne"]]

#print(nom_age_sex_moyenne)

nom_age_sex_moyenne_hommes = nom_age_sex_moyenne[nom_age_sex_moyenne["Sexe"]=="Homme"]

#print(nom_age_sex_moyenne_hommes)

nom_age_sex_moyenne_femmes = nom_age_sex_moyenne[nom_age_sex_moyenne["Sexe"] == "Femme"]

#print(nom_age_sex_moyenne_femmes)

nom_age_sex_moyenne_femmes_plus_10 = nom_age_sex_moyenne_femmes[nom_age_sex_moyenne_femmes["Moyenne"] >= 10 ]
#print(nom_age_sex_moyenne_femmes_plus_10)
#print(nom_age_sex_moyenne_femmes[nom_age_sex_moyenne_femmes["Moyenne"] < 10 ])

#print(nom_age_sex_moyenne_femmes_plus_10["Moyenne"].min())

#print(nom_age_sex_moyenne_femmes_plus_10.max())
#print(nom_age_sex_moyenne_femmes_plus_10["Moyenne"].max())

nom_age_sex_moyenne_16 = nom_age_sex_moyenne[
    (nom_age_sex_moyenne["Moyenne"] >= 16) & (nom_age_sex_moyenne["Moyenne"] < 17)
]
print(nom_age_sex_moyenne_16)

nom_age_sex_moyenne_16.to_csv("nom_age_sex_moyenne_16.csv")
```