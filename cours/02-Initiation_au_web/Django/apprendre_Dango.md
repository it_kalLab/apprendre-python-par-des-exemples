### Apprendre Django : Un Focus Complet

Django est un framework web en Python, conçu pour simplifier le développement d’applications web complexes. Nous allons nous concentrer sur les concepts clés suivants : **Introduction à Django**, **Structure d’un Projet Django**, **Vues et Templates**, **Modèles et Bases de Données**, **Formulaires et Validation**, et **Fichiers Statics et Médias**.

---

### 1. **Introduction à Django**

- **Qu'est-ce que Django ?**  
  Django est un framework web de haut niveau en Python qui encourage un développement rapide avec une approche pragmatique et propre. Il permet de construire des applications web tout en respectant des principes comme la réutilisation de code, la modularité et la rapidité de développement.

- **Pourquoi utiliser Django ?**
  - **Rapidité** : Django permet de créer des applications web complexes rapidement.
  - **Sécurité** : Il inclut des mécanismes intégrés pour sécuriser vos applications (protection contre les attaques par injection SQL, XSS, CSRF, etc.).
  - **Scalabilité** : Django est utilisé par des entreprises comme Instagram, Pinterest, et Mozilla, ce qui prouve sa capacité à gérer des applications de grande envergure.
  
  **Installation de Django** :
  - Installez Django via `pip` :
    ```bash
    pip install django
    ```
  - Vérifiez l'installation :
    ```bash
    python -m django --version
    ```

---

### 2. **Structure d'un Projet Django**

Lorsque vous créez un projet Django, la structure suivante est générée :

- **Commandes pour démarrer un projet** :
  ```bash
  django-admin startproject monprojet
  cd monprojet
  python manage.py runserver
  ```
  
- **Fichiers et dossiers clés** :
  - `manage.py` : Utilisé pour interagir avec le projet (lancer le serveur, effectuer des migrations, etc.).
  - Dossier `monprojet/` : Contient les fichiers de configuration du projet.
    - `settings.py` : Contient les configurations globales (base de données, sécurité, etc.).
    - `urls.py` : Gère les routes de l’application.
    - `wsgi.py` : Interface pour le déploiement en production.
  
- **Création d'une application** :
  Une application dans Django est un sous-module de votre projet qui gère une fonctionnalité spécifique :
  ```bash
  python manage.py startapp monapplication
  ```

---

### 3. **Vues et Templates**

- **Vues** :
  Les vues définissent la logique qui relie les modèles et les templates. Elles reçoivent les requêtes HTTP, interagissent avec les modèles si nécessaire, et renvoient une réponse.

  - Exemple d’une vue dans `views.py` :
    ```python
    from django.http import HttpResponse

    def index(request):
        return HttpResponse("Bienvenue sur mon site web!")
    ```

- **Templates** :
  Les templates gèrent l'affichage des données dans le navigateur. Django utilise le moteur de template intégré pour séparer la logique de présentation du reste du code.

  - Exemple de template `index.html` :
    ```html
    <h1>Bienvenue sur mon site web!</h1>
    <p>Ceci est un exemple de template.</p>
    ```

- **Lier une vue à un template** :
  Utilisation de la fonction `render` pour relier une vue à un template :
  ```python
  from django.shortcuts import render

  def index(request):
      return render(request, 'index.html')
  ```

- **Configuration des URLs** :
  Chaque vue doit être associée à une URL dans `urls.py` :
  ```python
  from django.urls import path
  from . import views

  urlpatterns = [
      path('', views.index, name='index'),
  ]
  ```

---

### 4. **Modèles et Bases de Données**

Django utilise un ORM (Object-Relational Mapping) pour interagir avec les bases de données sans écrire de SQL directement.

- **Définir un modèle** :
  Les modèles sont des classes Python qui correspondent à des tables de la base de données. Exemple de modèle dans `models.py` :
  ```python
  from django.db import models

  class Article(models.Model):
      titre = models.CharField(max_length=200)
      contenu = models.TextField()
      date_pub = models.DateTimeField(auto_now_add=True)
  ```

- **Migrations** :
  Après avoir défini ou modifié un modèle, vous devez créer et appliquer des migrations pour synchroniser la base de données :
  ```bash
  python manage.py makemigrations
  python manage.py migrate
  ```

- **Interagir avec les données** :
  Vous pouvez interagir avec les objets du modèle via l’ORM :
  ```python
  # Créer un nouvel article
  article = Article.objects.create(titre="Mon premier article", contenu="Ceci est le contenu")
  
  # Lire les articles
  articles = Article.objects.all()
  ```

---

### 5. **Formulaires et Validation**

Django propose un module pour gérer les formulaires, qui facilite la collecte et la validation des données.

- **Créer un formulaire** :
  Utilisez les classes `Form` ou `ModelForm` pour définir un formulaire. Exemple de formulaire simple :
  ```python
  from django import forms

  class ContactForm(forms.Form):
      nom = forms.CharField(max_length=100)
      email = forms.EmailField()
      message = forms.CharField(widget=forms.Textarea)
  ```

- **Valider les données** :
  Django gère automatiquement la validation des champs (ex : les champs `EmailField` vérifient la validité de l’email).
  
  Exemple de traitement d’un formulaire dans une vue :
  ```python
  def contact(request):
      if request.method == 'POST':
          form = ContactForm(request.POST)
          if form.is_valid():
              # Process the data
              nom = form.cleaned_data['nom']
              email = form.cleaned_data['email']
              message = form.cleaned_data['message']
      else:
          form = ContactForm()
      return render(request, 'contact.html', {'form': form})
  ```

---

### 6. **Fichiers Statics et Médias**

- **Fichiers statiques (CSS, JS, Images)** :
  Les fichiers statiques sont des fichiers comme des images, du CSS ou du JavaScript, qui ne changent pas dynamiquement.
  
  - **Configurer les fichiers statiques** :
    Dans `settings.py` :
    ```python
    STATIC_URL = '/static/'
    STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]
    ```

  - **Utiliser des fichiers statiques dans un template** :
    ```html
    {% load static %}
    <link rel="stylesheet" href="{% static 'css/styles.css' %}">
    ```

- **Fichiers médias (upload)** :
  Les fichiers médias sont les fichiers téléchargés par les utilisateurs (photos, documents, etc.).
  
  - **Configurer les fichiers médias** :
    Dans `settings.py` :
    ```python
    MEDIA_URL = '/media/'
    MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
    ```

  - **Gestion des fichiers téléchargés via un formulaire** :
    Lorsque vous créez un formulaire pour télécharger des fichiers, assurez-vous de configurer correctement le formulaire et la vue :
    ```html
    <form method="POST" enctype="multipart/form-data">
      {% csrf_token %}
      {{ form.as_p }}
      <button type="submit">Envoyer</button>
    </form>
    ```

---

Avec ces bases, vous pouvez construire des applications Django robustes et bien structurées. Vous avez désormais un bon aperçu des fondamentaux nécessaires pour maîtriser Django.