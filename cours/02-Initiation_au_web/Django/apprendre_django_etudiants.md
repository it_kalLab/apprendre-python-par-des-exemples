Pour apprendre à utiliser Django en manipulant cette liste de données, nous allons créer une application qui permet de gérer des **"Étudiants"** avec les champs `ID`, `Nom`, `Age`, `Sexe`, `Filière`, et `Moyenne`. Voici un tutoriel complet.


### **Étape 1 : Préparer l'environnement**
1. Installez Django :
   ```bash
   pip install django
   ```

2. Créez un projet Django :
   ```bash
   django-admin startproject gestion_etudiants
   cd gestion_etudiants
   ```

3. Créez une application appelée `etudiants` :
   ```bash
   python manage.py startapp etudiants
   ```

4. Ajoutez `etudiants` à `INSTALLED_APPS` dans `settings.py` :
   ```python
   INSTALLED_APPS = [
       'django.contrib.admin',
       'django.contrib.auth',
       'django.contrib.contenttypes',
       'django.contrib.sessions',
       'django.contrib.messages',
       'django.contrib.staticfiles',
       'etudiants',  # Ajout de l'application
   ]
   ```

5. Lancez le serveur pour vérifier :
   ```bash
   python manage.py runserver
   ```

   Accédez à [http://127.0.0.1:8000](http://127.0.0.1:8000) pour voir que le projet fonctionne.

---

### **Étape 2 : Créer le modèle pour les étudiants**
Dans le fichier `etudiants/models.py`, définissez le modèle `Etudiant` :

```python
from django.db import models

class Etudiant(models.Model):
    SEXE_CHOICES = [
        ('Homme', 'Homme'),
        ('Femme', 'Femme'),
    ]

    FILIERE_CHOICES = [
        ('Mathématiques', 'Mathématiques'),
        ('Physique', 'Physique'),
        ('Informatique', 'Informatique'),
        ('Économie', 'Économie'),
    ]

    id = models.AutoField(primary_key=True)
    nom = models.CharField(max_length=200)
    age = models.PositiveIntegerField()
    sexe = models.CharField(max_length=10, choices=SEXE_CHOICES)
    filiere = models.CharField(max_length=20, choices=FILIERE_CHOICES)
    moyenne = models.DecimalField(max_digits=4, decimal_places=2)

    def __str__(self):
        return self.nom
```


### **Étape 3 : Migrer la base de données**
Créez et appliquez les migrations pour ajouter ce modèle à la base de données :
```bash
python manage.py makemigrations
python manage.py migrate
```


### **Étape 4 : Ajouter les étudiants via l'interface d'administration**
1. Dans `etudiants/admin.py`, enregistrez le modèle `Etudiant` pour l'interface admin :
   ```python
   from django.contrib import admin
   from .models import Etudiant

   @admin.register(Etudiant)
   class EtudiantAdmin(admin.ModelAdmin):
       list_display = ('id', 'nom', 'age', 'sexe', 'filiere', 'moyenne')
   ```

2. Créez un superutilisateur :
   ```bash
   python manage.py createsuperuser
   ```

3. Accédez à l'interface admin à [http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin) pour ajouter des étudiants.


### **Étape 5 : Pré-remplir les données dans la base**
Utilisez une **migration personnalisée** pour insérer les données dans la base de données :

1. Créez une vue(views.py) :
   ```python
   """
   importer des données depuis le fichier etudiants-1.csv vers une base de données (sqlite3)
   """
   def import_data(request):
    # Importer la librairie Pandas
    import pandas as pd
    
    # Lire le contenu du fichier etudiants-1.csv
    etudiant_df = pd.read_csv("etudiants-1.csv")

    # Assigner les données au sein de la base de données
    # Parcourir chaque ligne du DataFrame et sauvegarder dans la base
    for _, row in df.iterrows():
        Student.objects.create(
            id=row['ID'],
            nom=row['Nom'],
            age=row['Age'],
            sexe=row['Sexe'],
            filiere=row['Filiere'],
            moyenne=row['Moyenne']
        )
    
    return render()



   ```

2. Créer la vue

   ```python
   def display_data(request):
    return render()
   
   ```

3. Appliquez cette migration :
   ```bash
   python manage.py migrate
   ```


### **Étape 6 : Créer une vue et des templates**
1. Dans `etudiants/views.py`, ajoutez les vues suivantes :
   ```python
   from django.shortcuts import render
   from .models import Etudiant

   def liste_etudiants(request):
       etudiants = Etudiant.objects.all()
       return render(request, 'etudiants/liste.html', {'etudiants': etudiants})
   ```

2. Créez un fichier `urls.py` dans l'application `etudiants` :
   ```python
   from django.urls import path
   from . import views

   urlpatterns = [
       path('', views.liste_etudiants, name='liste_etudiants'),
   ]
   ```

3. Incluez ces URLs dans le fichier `gestion_etudiants/urls.py` :
   ```python
   from django.contrib import admin
   from django.urls import path, include

   urlpatterns = [
       path('admin/', admin.site.urls),
       path('', include('etudiants.urls')),
   ]
   ```

4. Créez un template pour afficher la liste des étudiants dans `templates/etudiants/liste.html` :
   ```html
   <!DOCTYPE html>
   <html>
   <head>
       <title>Liste des Étudiants</title>
   </head>
   <body>
       <h1>Liste des Étudiants</h1>
       <table border="1">
           <thead>
               <tr>
                   <th>ID</th>
                   <th>Nom</th>
                   <th>Âge</th>
                   <th>Sexe</th>
                   <th>Filière</th>
                   <th>Moyenne</th>
               </tr>
           </thead>
           <tbody>
               {% for etudiant in etudiants %}
               <tr>
                   <td>{{ etudiant.id }}</td>
                   <td>{{ etudiant.nom }}</td>
                   <td>{{ etudiant.age }}</td>
                   <td>{{ etudiant.sexe }}</td>
                   <td>{{ etudiant.filiere }}</td>
                   <td>{{ etudiant.moyenne }}</td>
               </tr>
               {% endfor %}
           </tbody>
       </table>
   </body>
   </html>
   ```

### **Étape 7 : Tester l'application**
1. Lancez le serveur :
   ```bash
   python manage.py runserver
   ```

2. Accédez à [http://127.0.0.1:8000](http://127.0.0.1:8000) pour voir la liste des étudiants.
