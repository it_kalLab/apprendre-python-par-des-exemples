### Chapitre 8 : Avancé
## Objectif : Maîtriser des concepts avancés de Python pour des applications plus performantes et efficaces

### Expressions Génératrices et Itérateurs

#### Création et Utilisation des Générateurs

Les générateurs permettent de créer des itérateurs de manière plus concise et efficiente grâce au mot-clé `yield`. Ils sont particulièrement utiles pour travailler avec de grandes quantités de données sans les charger toutes en mémoire.

**Exemple :**
```python
def generate_numbers(limit):
    for i in range(limit):
        yield i

# Utilisation du générateur
for number in generate_numbers(5):
    print(number)  # Affiche 0, 1, 2, 3, 4
```

Les expressions génératrices permettent de créer des générateurs de manière plus compacte, similaire aux compréhensions de listes.

**Exemple :**
```python
squares = (x * x for x in range(10))
for square in squares:
    print(square)  # Affiche 0, 1, 4, 9, 16, 25, 36, 49, 64, 81
```

#### Itérateurs et Protocoles d'Itération

Un itérateur est un objet qui implémente les méthodes `__iter__()` et `__next__()`. Le protocole d'itération permet de parcourir des éléments un par un.

**Exemple :**
```python
class MyIterator:
    def __init__(self, start, end):
        self.current = start
        self.end = end

    def __iter__(self):
        return self

    def __next__(self):
        if self.current < self.end:
            self.current += 1
            return self.current
        else:
            raise StopIteration

# Utilisation de l'itérateur
for number in MyIterator(1, 5):
    print(number)  # Affiche 2, 3, 4, 5
```

### 17. Programmation Fonctionnelle

#### Fonctions `map`, `filter` et `reduce`

Les fonctions `map`, `filter` et `reduce` permettent d'appliquer des opérations fonctionnelles sur des collections de données.

**Exemple de `map` :**
```python
numbers = [1, 2, 3, 4, 5]
squares = list(map(lambda x: x * x, numbers))
print(squares)  # Affiche [1, 4, 9, 16, 25]
```

**Exemple de `filter` :**
```python
numbers = [1, 2, 3, 4, 5]
evens = list(filter(lambda x: x % 2 == 0, numbers))
print(evens)  # Affiche [2, 4]
```

**Exemple de `reduce` :**
```python
from functools import reduce

numbers = [1, 2, 3, 4, 5]
sum = reduce(lambda x, y: x + y, numbers)
print(sum)  # Affiche 15
```

#### Compréhension des Expressions Lambda

Les fonctions lambda sont des fonctions anonymes, courtes et concises.

**Exemple :**
```python
add = lambda x, y: x + y
print(add(3, 5))  # Affiche 8

numbers = [1, 2, 3, 4, 5]
squares = list(map(lambda x: x * x, numbers))
print(squares)  # Affiche [1, 4, 9, 16, 25]
```

### 18. Threads et Programmation Concurrente

#### Introduction à la Programmation Multi-threading

Le multi-threading permet d'exécuter plusieurs threads (flux d'exécution) simultanément, facilitant la concurrence dans les programmes.

#### Utilisation du Module `threading`

Le module `threading` fournit des outils pour créer et gérer des threads en Python.

**Exemple :**
```python
import threading

def print_numbers():
    for i in range(5):
        print(i)

thread1 = threading.Thread(target=print_numbers)
thread2 = threading.Thread(target=print_numbers)

thread1.start()
thread2.start()

thread1.join()
thread2.join()
```

#### Introduction à l'Asynchrone avec `asyncio`

Le module `asyncio` permet la programmation asynchrone, où les tâches peuvent être exécutées de manière concurrente sans bloquer le flux d'exécution.

**Exemple :**
```python
import asyncio

async def print_numbers():
    for i in range(5):
        print(i)
        await asyncio.sleep(1)

async def main():
    await asyncio.gather(print_numbers(), print_numbers())

asyncio.run(main())
```

### Conclusion

Ce chapitre a couvert des concepts avancés de Python, y compris les générateurs, itérateurs, programmation fonctionnelle, et la programmation concurrente avec les threads et l'asynchronisme. Ces compétences vous permettront de créer des applications Python plus performantes et efficaces.
