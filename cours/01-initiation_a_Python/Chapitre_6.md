### Chapitre 6 : Gestion des Fichiers

## Objectif : Apprendre à lire, écrire et gérer des fichiers en Python

### 13. Lecture et Écriture de Fichiers

#### Ouverture et Fermeture de Fichiers

En Python, l'ouverture et la fermeture de fichiers se font avec la fonction `open()`. Les fichiers doivent être fermés après utilisation pour libérer les ressources.

**Exemple :**
```python
# Ouverture d'un fichier en mode lecture
file = open('example.txt', 'r')
# Fermeture du fichier
file.close()
```

L'utilisation du mot-clé `with` garantit que le fichier est correctement fermé après son utilisation, même en cas d'exception.

**Exemple :**
```python
with open('example.txt', 'r') as file:
    content = file.read()
    print(content)
# Le fichier est automatiquement fermé ici
```

#### Lecture et Écriture de Fichiers Texte

**Lecture de Fichiers Texte :**
- `read()`: Lit tout le contenu du fichier.
- `readline()`: Lit une ligne à la fois.
- `readlines()`: Lit toutes les lignes et les retourne sous forme de liste.

**Exemples :**
```python
with open('example.txt', 'r') as file:
    content = file.read()
    print(content)

with open('example.txt', 'r') as file:
    line = file.readline()
    while line:
        print(line.strip())
        line = file.readline()

with open('example.txt', 'r') as file:
    lines = file.readlines()
    for line in lines:
        print(line.strip())
```

**Écriture de Fichiers Texte :**
- `write()`: Écrit une chaîne de caractères dans le fichier.
- `writelines()`: Écrit une liste de chaînes de caractères dans le fichier.

**Exemples :**
```python
with open('output.txt', 'w') as file:
    file.write("Ceci est une ligne de texte.\n")
    file.write("Ceci est une autre ligne de texte.\n")

with open('output.txt', 'w') as file:
    lines = ["Ligne 1\n", "Ligne 2\n", "Ligne 3\n"]
    file.writelines(lines)
```

#### Gestion des Exceptions Liées aux Fichiers

Lors de l'ouverture et de la manipulation de fichiers, des erreurs peuvent se produire. Il est important de gérer ces exceptions pour rendre votre programme plus robuste.

**Exemple :**
```python
try:
    with open('non_existent_file.txt', 'r') as file:
        content = file.read()
except FileNotFoundError:
    print("Le fichier n'existe pas.")
except Exception as e:
    print(f"Une erreur est survenue : {e}")
```

### 14. Travail avec les Fichiers CSV et JSON

#### Utilisation du Module `csv`

Le module `csv` facilite la lecture et l'écriture de fichiers CSV (Comma-Separated Values).

**Lecture de Fichiers CSV :**
```python
import csv

with open('data.csv', 'r') as file:
    reader = csv.reader(file)
    for row in reader:
        print(row)
```

**Écriture de Fichiers CSV :**
```python
import csv

data = [
    ['Nom', 'Âge'],
    ['Alice', 30],
    ['Bob', 25]
]

with open('output.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(data)
```

#### Utilisation du Module `json`

Le module `json` permet de travailler facilement avec les fichiers JSON (JavaScript Object Notation).

**Lecture de Fichiers JSON :**
```python
import json

with open('data.json', 'r') as file:
    data = json.load(file)
    print(data)
```

**Écriture de Fichiers JSON :**
```python
import json

data = {
    'name': 'Alice',
    'age': 30,
    'city': 'Paris'
}

with open('output.json', 'w') as file:
    json.dump(data, file, indent=4)
```

### Exercices
Trouvez ici les [exercices](Exercices_chap_6.md)

### Conclusion

Ce chapitre a couvert les bases de la gestion des fichiers en Python, y compris l'ouverture et la fermeture de fichiers, la lecture et l'écriture de fichiers texte, ainsi que la gestion des exceptions. Vous avez également appris à travailler avec des fichiers CSV et JSON en utilisant les modules `csv` et `json`. Ces compétences sont essentielles pour manipuler et gérer les données dans des projets réels.

-------------------------------------

Chapitre 6 : Avancé et pratique continue
Objectif : Approfondir les connaissances et travailler sur des projets plus complexes

1. Gestion des exceptions :
   - Try, except, finally.
   - Création de vos propres exceptions.

2. Générateurs et itérateurs :
   - Comprendre les itérateurs.
   - Utilisation des générateurs (`yield`).

3. Travail sur des projets plus complexes :
   - Applications de gestion de bases de données avec SQLite et SQLAlchemy.
   - Analyse de données avec `pandas` et visualisation avec `matplotlib` ou `seaborn`.

4. Contribuer à des projets open source :
   - Utiliser GitLab pour collaborer.
   - Comprendre le workflow Git (clone, commit, push, pull request).


