# Chapitre II : Structures de données et manipulation

## Objectif : Maîtriser les structures de données courantes et leurs manipulations

### 1. Listes
#### Création, manipulation (ajout, suppression, modification)
- **Création d'une liste** :
  ```python
  ma_liste = [1, 2, 3, 4, 5]
  ```

- **Ajout d'éléments** :
  ```python
  ma_liste.append(6)  # Ajoute un élément à la fin de la liste
  ma_liste.insert(0, 0)  # Ajoute un élément à l'indice spécifié
  ```

- **Suppression d'éléments** :
  ```python
  ma_liste.remove(3)  # Supprime la première occurrence de l'élément
  del ma_liste[2]  # Supprime l'élément à l'indice spécifié
  ```

- **Modification d'éléments** :
  ```python
  ma_liste[1] = 10  # Modifie l'élément à l'indice spécifié
  ```

#### Listes imbriquées, compréhension de listes
- **Listes imbriquées** :
  ```python
  liste_imbriquee = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
  ```

- **Compréhension de listes** :
  ```python
  carres = [x**2 for x in range(10)]  # Génère une liste des carrés de 0 à 9
  ```

### 2. Tuples et ensembles
#### Différences entre listes et tuples
- **Liste** (modifiable) :
  ```python
  ma_liste = [1, 2, 3]
  ma_liste[0] = 0  # Modifiable
  ```

- **Tuple** (non modifiable) :
  ```python
  mon_tuple = (1, 2, 3)
  ```

#### Utilisation des ensembles pour des opérations ensemblistes
- **Création d'un ensemble** :
  ```python
  mon_ensemble = {1, 2, 3, 4, 5}
  ```

- **Opérations ensemblistes** :
  ```python
  ensemble_A = {1, 2, 3}
  ensemble_B = {3, 4, 5}

  union = ensemble_A | ensemble_B  # Union
  intersection = ensemble_A & ensemble_B  # Intersection
  difference = ensemble_A - ensemble_B  # Différence
  ```

### 3. Dictionnaires
#### Paires clé-valeur
- **Création d'un dictionnaire** :
  ```python
  mon_dict = {"nom": "Alice", "age": 25}
  ```

#### Méthodes de manipulation des dictionnaires
- **Accéder à une valeur** :
  ```python
  age = mon_dict["age"]
  ```

- **Ajouter ou modifier une paire clé-valeur** :
  ```python
  mon_dict["ville"] = "Paris"  # Ajoute une nouvelle clé-valeur
  mon_dict["age"] = 26  # Modifie la valeur associée à la clé "age"
  ```

- **Supprimer une paire clé-valeur** :
  ```python
  del mon_dict["age"]
  ```

### 4. Structures de contrôle
#### Conditions : `if`, `elif`, `else`
- **Conditions de base** :
  ```python
  x = 10
  if x > 0:
      print("x est positif")
  elif x == 0:
      print("x est zéro")
  else:
      print("x est négatif")
  ```

#### Boucles : `for`, `while`
- **Boucle `for`** :
  ```python
  for i in range(5):
      print(i)
  ```

- **Boucle `while`** :
  ```python
  i = 0
  while i < 5:
      print(i)
      i += 1
  ```

### 5. Manipulation de fichiers
#### Lecture et écriture de fichiers

- **Lecture d'un fichier** :
  ```python
  open("mon_fichier.txt", "r") as fichier:
      contenu = fichier.read()
      fichier.close()
  print(contenu)
  ```

- **Lecture d'un fichier** :
  ```python
  with open("mon_fichier.txt", "r") as fichier:
      contenu = fichier.read()
      print(contenu)
  ```

- **Écriture dans un fichier** :
  ```python
  with open("mon_fichier.txt", "w") as fichier:
      fichier.write("Bonjour, monde!")
  ```

### 6. Fonctions
#### Définir et appeler des fonctions avec `def`
- **Définition d'une fonction** :
  ```python
  def saluer(nom):
      print(f"Bonjour, {nom}!")
  ```

- **Appel d'une fonction** :
  ```python
  saluer("Alice")
  ```

#### Paramètres et valeurs de retour
- **Fonction avec paramètres et valeur de retour** :
  ```python
  def addition(a, b):
      return a + b

  resultat = addition(5, 3)
  print(resultat)  # Affiche 8
  ```

### Exercices
Trouvez ici les [exercices](Exercices_chap_2.md) 