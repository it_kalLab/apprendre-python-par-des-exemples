`
# Chapitre I: Prise en Main de Python
## I. Installation de Python sur différentes plateformes (Windows, Linux, macOS)
### 1. Installation de Python sur Windows
- Rendez-vous sur le site officiel (https://www.python.org) de Python.
- Allez dans la section "Downloads" et cliquez sur "Download Python" pour obtenir l'installateur.

#### Exécution de l'installateur :
- Ouvrez le fichier téléchargé (.exe).
- Cochez la case "Add Python to PATH" en bas de la fenêtre.
- Cliquez sur "Install Now" pour une installation par défaut ou sur "Customize installation" pour des options avancées.

#### Vérification de l'installation :
- Ouvrez l'invite de commandes (CMD).
- Tapez `python --version` et appuyez sur Entrée. Vous devriez voir la version de Python installée.

### 2. Installation de Python sur Linux
#### Debian/Ubuntu :
- Ouvrez un terminal.
- Tapez `sudo apt update` pour mettre à jour les listes de paquets.
- Tapez `sudo apt install python3` pour installer Python 3.
- Tapez `sudo apt install python3-pip` pour installer pip (gestionnaire de paquets Python).

#### Fedora :
- Ouvrez un terminal.
- Tapez `sudo dnf install python3` pour installer Python 3.
- Tapez `sudo dnf install python3-pip` pour installer pip.

#### Arch Linux :
- Ouvrez un terminal.
- Tapez `sudo pacman -S python` pour installer Python 3.
- Tapez `sudo pacman -S python-pip` pour installer pip.

#### Vérification de l'installation :
- Ouvrez un terminal.
- Tapez `python3 --version` et appuyez sur Entrée. Vous devriez voir la version de Python installée.

### 3. Installation de Python sur macOS
#### Téléchargement de l'installateur :
- Rendez-vous sur le site officiel de Python : [python.org](https://www.python.org).
- Allez dans la section "Downloads" et cliquez sur "Download Python" pour obtenir l'installateur pour macOS.

#### Exécution de l'installateur :
- Ouvrez le fichier téléchargé (.pkg).
- Suivez les instructions à l'écran pour installer Python.

#### Installation via Homebrew (alternative) :
Si vous utilisez Homebrew, 
- ouvrez un terminal.
- Tapez `brew install python` pour installer la dernière version de Python.

#### Vérification de l'installation :
- Ouvrez un terminal.
- Tapez `python3 --version` et appuyez sur Entrée. Vous devriez voir la version de Python installée.

### Résumé
Après l'installation, vous pouvez utiliser Python en ouvrant un terminal (ou l'invite de commandes sur Windows) et en tapant `python` ou `python3` selon votre système d'exploitation. Vous pouvez également installer des packages supplémentaires avec pip en utilisant la commande `pip install nom_du_package`. Python est maintenant prêt à être utilisé pour vos projets de développement.

## II. Éditeurs de texte
Un éditeur de texte permet aux développeurs de créer, éditer et sauvegarder des fichiers Python (.py) avec une syntaxe correctement formatée.

Parmi les éditeurs de texte populaires pour programmer en Python, on trouve Visual Studio Code, Sublime Text, Atom, et Notepad++. Ces éditeurs sont appréciés pour leur légèreté, leur rapidité, et leur capacité à être personnalisés avec des extensions et des plugins spécifiques à Python.

## III. Exécuter votre premier programme Python
Il est possible d'exécuter du code Python à partir d'un **terminal** en y entrant la commande `python3` pour accéder à l'interpréteur. Vous pouvez ensuite y écrire le code, en terminant chaque instruction par la touche **Entrée** du clavier. 

Par exemple :
```python
print("La vie est belle")
```

## IV. Variables
En Python, une variable est un espace de stockage nommé qui peut contenir une valeur. La valeur de la variable peut changer au cours de l'exécution du programme.

### Déclaration et affectation :
En Python, vous pouvez créer une variable et lui attribuer une valeur en une seule ligne.
  ```python
  x = 10
  y = 3.14
  nom = "Alice"
  est_actif = True
  ```

### Nommer les variables
Les noms de variables doivent commencer par une lettre ou un soulignement (_), et peuvent contenir des lettres, des chiffres et des soulignements. Ils sont sensibles à la casse (`nom` et `Nom` sont deux variables différentes).

### Types de Données de Base
Entiers (int) : représentent des nombres entiers, positifs ou négatifs, sans partie décimale.
   ```python
   x = 42
   y = -5
   ```

Flottants (float) : représentent des nombres réels avec une partie décimale.
   ```python
   pi = 3.14159
   ```

Chaînes de caractères (str) : représentent des séquences de caractères utilisées pour stocker du texte.
   ```python
   message = "Bonjour, monde!"
   ```

Booléens (bool) : représentent des valeurs logiques, soit `True` soit `False`.
   ```python
   est_vrai = True
   est_faux = False
   ```

### Opérations Arithmétiques
Les opérations arithmétiques de base peuvent être effectuées avec les types `int` et `float` :

- Addition (+) :
  ```python
  a = 10 + 5    a vaut 15
  ```

- Soustraction (-) :
  ```python
  b = 10 - 5    b vaut 5
  ```

- Multiplication (*) :
  ```python
  c = 10*5    c vaut 50
  ```

- Division (/) :
  ```python
  d = 10 / 2    d vaut 5.0
  ```

- Division entière (//) :
  ```python
  e = 10 // 3   e vaut 3
  ```

- Modulo (%) :
  ```python
  f = 10 % 3    f vaut 1
  ```

- Exponentiation () :
  ```python
  g = 2  3    g vaut 8
  ```
#### Exercices
Cliquez ici pour voir les [exercices](Exercices_chap_1.md)


### Manipulation de Chaînes de Caractères
Les chaînes de caractères (`str`) en Python offrent de nombreuses méthodes pour la manipulation de texte :

- Concaténation :
  ```python
  salut = "Bonjour"
  monde = "monde"
  message = salut + ", " + monde + "!"   "Bonjour, monde!"
  ```

- Répétition :
  ```python
  rires = "ha"  3   "hahaha"
  ```

- Accès aux caractères :
  ```python
  lettre = message[0]   "B"
  ```

- Tranchage (slicing) :
  ```python
  partie = message[0:7]   "Bonjour"
  ```

- Longueur d'une chaîne :
  ```python
  longueur = len(message)   13
  ```

- Méthodes de chaînes :
  ```python
  upper_message = message.upper()   "BONJOUR, MONDE!"
  lower_message = message.lower()   "bonjour, monde!"
  ```

### Listes (list)
Les listes sont des collections ordonnées et modifiables d'éléments. Elles peuvent contenir des éléments de types différents.

- Création et accès :
  ```python
  fruits = ["pomme", "banane", "cerise"]
  premier_fruit = fruits[0]   "pomme"
  ```

- Modification d'éléments :
  ```python
  fruits[1] = "orange"
  ```

- Ajout d'éléments :
  ```python
  fruits.append("mangue")
  ```

- Suppression d'éléments :
  ```python
  fruits.remove("cerise")
  ```

- Longueur de la liste :
  ```python
  longueur = len(fruits)   #3
  ```

### Tuples (tuple)
Les tuples sont similaires aux listes, mais ils sont immuables (leurs éléments ne peuvent pas être modifiés après leur création).

- Création et accès :
  ```python
  point = (10, 20)
  x = point[0]   #10
  y = point[1]   #20
  ```

- Immutabilité :
  ```python
   point[0] = 15   Cela provoquerait une erreur
  ```

- Utilisation courante :
  Les tuples sont souvent utilisés pour représenter des structures de données simples et immuables.

### Dictionnaires (dict)
Les dictionnaires sont des collections non ordonnées de paires clé-valeur. Chaque clé doit être unique.

- Création et accès :
  ```python
  etudiant = {"nom": "Alice", "âge": 25, "cours": "Mathématiques"}
  nom = etudiant["nom"]   "Alice"
  ```

- Modification de valeurs :
  ```python
  etudiant["âge"] = 26
  ```

- Ajout de paires clé-valeur :
  ```python
  etudiant["adresse"] = "123 Rue Agbé lé N'ko"
  ```

- Suppression de paires clé-valeur :
  ```python
  del etudiant["cours"]
  ```

- Longueur du dictionnaire :
  ```python
  longueur = len(etudiant)   3
  ```

## Conclusion

Les variables et les types de données sont des concepts fondamentaux en Python. Outre les types de base (`int`, `float`, `str`, `bool`), les listes, tuples et dictionnaires offrent des moyens puissants et flexibles pour organiser et manipuler des collections de données. Comprendre ces concepts est essentiel pour écrire des programmes efficaces et robustes en Python.



