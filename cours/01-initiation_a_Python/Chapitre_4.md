# Chapitre 4 : Programmation orientée objet (POO)
## Objectif : Comprendre et appliquer les concepts de la POO en Python
### 1. Classes et Objets

Les classes et les objets sont les bases de la programmation orientée objet. Une classe est un modèle pour créer des objets. Un objet est une instance d'une classe.

**Définir une Classe et Créer des Objets :**

Une classe est définie à l'aide du mot-clé `class`. Les objets sont créés en appelant la classe.

**Exemple :**
```python
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

# Création d'objets
person1 = Person("Alice", 30)
person2 = Person("Bob", 25)

print(person1.name, person1.age)  # Alice 30
print(person2.name, person2.age)  # Bob 25
```

**Attributs et Méthodes :**

Les attributs sont des variables appartenant à une classe, et les méthodes sont des fonctions définies à l'intérieur d'une classe.

**Exemple :**
```python
class Dog:
    def __init__(self, name, breed):
        self.name = name
        self.breed = breed

    def bark(self):
        print(f"{self.name} says Woof!")

dog1 = Dog("Buddy", "Golden Retriever")
dog1.bark()  # Buddy says Woof!
```

### 2. Héritage

L'héritage permet de créer une nouvelle classe basée sur une classe existante. La classe existante est appelée superclasse ou classe parente, et la nouvelle classe est appelée sous-classe ou classe enfant.

**Concepts d'Héritage, Superclasses et Sous-classes :**

**Exemple :**
```python
class Animal:
    def __init__(self, name):
        self.name = name

    def speak(self):
        pass

class Dog(Animal):
    def speak(self):
        return f"{self.name} says Woof!"

class Cat(Animal):
    def speak(self):
        return f"{self.name} says Meow!"

dog = Dog("Buddy")
cat = Cat("Whiskers")

print(dog.speak())  # Buddy says Woof!
print(cat.speak())  # Whiskers says Meow!
```

**Utilisation de `super()` :**

Le mot-clé `super()` est utilisé pour appeler une méthode de la superclasse.

**Exemple :**
```python
class Animal:
    def __init__(self, name):
        self.name = name

class Dog(Animal):
    def __init__(self, name, breed):
        super().__init__(name)
        self.breed = breed

    def speak(self):
        return f"{self.name} says Woof! I'm a {self.breed}."

dog = Dog("Buddy", "Golden Retriever")
print(dog.speak())  # Buddy says Woof! I'm a Golden Retriever.
```
### 2.1 Exemple pratique
```python
class Animal:
    def __init__(self, nom, age):
        self.nom = nom
        self.age = age

    def jeMePresente(self):
        print("Je suis un animal")

    def jeParle(self):
        print("Je parle comme un animal")

class Chat(Animal):
    def jeMePresente(self):
        super().jeMePresente()
        print("Mais je suis un Chat")

    def jeParle(self):
        print("Meow Meow Meow")

class Chien(Animal):
    def jeMePresente(self):
        print("Mais je suis un Chien")

    def jeParle(self):
        print("Woff woff")

animal = Animal("Dambe", 19)
chat = Chat("Tom", 2)
chien = Chien("Qui sait?", 3)

#animal.jeMePresente()
#chat.jeMePresente()
chien.jeMePresente()
```

### 3. Encapsulation et Polymorphisme

**Encapsulation :**

L'encapsulation consiste à restreindre l'accès à certains attributs ou méthodes d'une classe. En Python, cela est réalisé par la convention de préfixer les attributs avec un underscore `_` (protégé) ou deux underscores `__` (privé).

**Exemple :**
```python
class BankAccount:
    def __init__(self, owner, balance):
        self.owner = owner
        self.__balance = balance  # Attribut privé

    def deposit(self, amount):
        self.__balance += amount

    def get_balance(self):
        return self.__balance

account = BankAccount("Alice", 1000)
account.deposit(500)
print(account.get_balance())  # 1500
# print(account.__balance)  # Erreur : l'attribut est privé
```

**Polymorphisme et Méthodes Redéfinies :**

Le polymorphisme permet d'utiliser une interface commune pour des objets de différents types. En Python, cela est souvent réalisé via des méthodes redéfinies (override).

**Exemple :**
```python
class Shape:
    def area(self):
        pass

class Rectangle(Shape):
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def area(self):
        return self.width * self.height

class Circle(Shape):
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        import math
        return math.pi * (self.radius ** 2)

shapes = [Rectangle(4, 5), Circle(3)]

for shape in shapes:
    print(shape.area())
```
Sortie :
```
20
28.274333882308138
```

### Exercices
Trouvez ici les [exercices](Exercices_chap_4.md)

### Conclusion

Ce chapitre a couvert les concepts fondamentaux de la programmation orientée objet en Python, y compris la définition et la création de classes et d'objets, l'héritage, l'encapsulation, et le polymorphisme. Grâce à ces concepts, vous pouvez structurer votre code de manière plus efficace et réutilisable.
