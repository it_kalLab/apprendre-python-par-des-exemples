# Chapitre III : Contrôle de Flux

## III.1. Conditions
Le contrôle de flux en Python permet de diriger l'exécution de votre programme en fonction de conditions spécifiques et de créer des boucles pour exécuter des blocs de code plusieurs fois. Les structures de contrôle de flux incluent les instructions conditionnelles (`if`, `elif`, `else`) et les boucles (`for`, `while`). Nous allons les explorer à travers des exemples concrets.

III.1.1. **Instruction `if`**

   L'instruction `if` permet d'exécuter un bloc de code uniquement si une condition est vraie.

   **Exemple :**
   ```python
   age = 18
   if age >= 18:
       print("Vous êtes majeur.")
   ```

III.1.2. **Instruction `if...else`**

   L'instruction `else` permet de définir un bloc de code à exécuter lorsque la condition du `if` est fausse.

   **Exemple :**
   ```python
   age = 16
   if age >= 18:
       print("Vous êtes majeur.")
   else:
       print("Vous êtes mineur.")
   ```

III.1.3. **Instruction `if...elif...else`**

   L'instruction `elif` permet de vérifier plusieurs conditions différentes.

   **Exemple :**
   ```python
   age = 18
   if age < 18:
       print("Vous êtes mineur.")
   elif age == 18:
       print("Vous venez de devenir majeur!")
   else:
       print("Vous êtes majeur.")
   ```

## III.2. Boucles

III.2.1. **Boucle `for`**

   La boucle `for` est utilisée pour itérer sur une séquence (comme une liste, un tuple ou une chaîne de caractères).

   **Exemple :**
   ```python
   fruits = ["pomme", "banane", "cerise"]
   for fruit in fruits:
       print(fruit)
   ```
   Sortie :
   ```
   pomme
   banane
   cerise
   ```

III.2.2. **Boucle `while`**

   La boucle `while` continue d'exécuter un bloc de code tant qu'une condition est vraie.

   **Exemple :**
   ```python
   count = 0
   while count < 5:
       print(count)
       count += 1
   ```
   Sortie :
   ```
   0
   1
   2
   3
   4
   ```

## III.3. Utilisation de `break` et `continue`

1. **Instruction `break`**

   L'instruction `break` permet de sortir prématurément d'une boucle.

   **Exemple :**
   ```python
   for i in range(10):
       if i == 5:
           break
       print(i)
   ```
   Sortie :
   ```
   0
   1
   2
   3
   4
   ```

2. **Instruction `continue`**

   L'instruction `continue` permet de passer à l'itération suivante de la boucle, en sautant le reste du code dans la boucle pour l'itération courante.

   **Exemple :**
   ```python
   for i in range(10):
       if i % 2 == 0:
           continue
       print(i)
   ```
   Sortie :
   ```
   1
   3
   5
   7
   9
   ```

## III.4. Compréhensions de Listes

Les compréhensions de listes offrent un moyen concis de créer des listes. Elles combinent boucles et conditions dans une syntaxe compacte.

**Exemple :**
```python
# Créer une liste des carrés des nombres pairs de 0 à 9
squares = [x ** 2 for x in range(10) if x % 2 == 0]
print(squares)
```
Sortie :
```
[0, 4, 16, 36, 64]
```

## III.5. Fonctions en Python

Les fonctions sont un moyen de structurer et d'organiser votre code en le rendant modulaire, réutilisable et plus facile à comprendre. Cette section couvre la définition et l'appel des fonctions, les paramètres et valeurs de retour, la portée des variables, ainsi que les fonctions lambda et les expressions génératrices.

### III.5.1. Définition et Appel de Fonctions

Une fonction en Python est définie à l'aide du mot-clé `def`, suivi du nom de la fonction et des parenthèses.

**Exemple :**
```python
def greet():
    print("Hello, Soussoukparogan!")

# Appel de la fonction
greet()
```
Sortie :
```
Hello, Soussoukparogan!
```

### III.5.2. Paramètres et Valeurs de Retour

Les fonctions peuvent accepter des paramètres pour personnaliser leur comportement et renvoyer des valeurs à l'aide du mot-clé `return`.

**Exemple avec paramètres :**
```python
def greet(name):
    print(f"Hello, {name}!")

greet("Mambo")
greet("Ossara")
```
Sortie :
```
Hello, Mambo!
Hello, Ossara!
```

**Exemple avec valeur de retour :**
```python
def add(a, b):
    return a + b

result = add(3, 5)
print(result)
```
Sortie :
```
8
```

### III.5.3. Portée des Variables

La portée d'une variable détermine où cette variable peut être utilisée. En Python, les variables peuvent avoir une portée locale ou globale.

**Exemple de portée locale :**
```python
def my_function():
    x = 10  # Variable locale
    print(x)

my_function()
# print(x)  # Erreur : x n'est pas défini en dehors de la fonction
```

**Exemple de portée globale :**
```python
x = 20  # Variable globale

def my_function():
    global x
    x = 10  # Modifie la variable globale
    print(x)

my_function()
print(x)  # La valeur de x a été modifiée par la fonction
```
Sortie :
```
10
10
```
### III.5.3 : Exemple
```python
# Variables globales
PI = 3.14

def calculAireCercle(r: float) -> float:
    aire = PI*(r*r)
    return aire

def calculAireCarre(cote: float) -> float:
    aire = cote * cote
    return aire


def perimetreDuCercle(r):
    print(r*2*PI)

aireCercle = calculAireCercle(5)
print(f"L'aire de mon cercle est {aireCercle}")
aireCarre = calculAireCarre(3.5)
print(f"L'aire de mon carre est {aireCarre}")

aireSurface = aireCercle + aireCarre
print(f"L'aire de ma surface est {aireSurface}")
```

### III.5.4. Fonctions Lambda et Expressions Génératrices

Les fonctions lambda sont des fonctions anonymes et compactes définies à l'aide du mot-clé `lambda`. Elles peuvent prendre plusieurs arguments mais ne peuvent contenir qu'une seule expression.

**Exemple de fonction lambda :**
```python
add = lambda x, y: x + y
print(add(3, 5))
```
Sortie :
```
8
```

Les expressions génératrices sont similaires aux compréhensions de listes mais génèrent des éléments à la demande (paresseusement) et sont définies à l'aide de parenthèses.

**Exemple d'expression génératrice :**
```python
squares = (x ** 2 for x in range(10))
for square in squares:
    print(square)
```
Sortie :
```
0
1
4
9
16
25
36
49
64
81
```

### Conclusion

Les fonctions en Python sont des blocs de code réutilisables qui permettent de structurer vos programmes de manière modulaire et efficace. En maîtrisant la définition et l'appel des fonctions, la gestion des paramètres et des valeurs de retour, la compréhension de la portée des variables, et l'utilisation des fonctions lambda et des expressions génératrices, vous serez capable de créer des programmes Python plus propres et plus performants.