# Demander à l'utilisateur de saisir deux nombres entiers
nombre1 = int(input("Entrez le premier nombre entier : "))
nombre2 = int(input("Entrez le deuxième nombre entier : "))

# Calculer le reste
reste = nombre1 % nombre2

# Afficher le résultat
print(f"Le reste de la division de {nombre1} par {nombre2} est {reste}.")
