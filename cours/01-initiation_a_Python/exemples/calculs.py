from math import sqrt


def resolutionEquationSecondDegre(a, b, c):
    # ax2 + bx + c = 0
    # a = 5, b = 4, c = -2 ==> 5x2 + 4x -2 = 0

    #calculer le discriminant
    disc = (b*b) - 4*(a*c)
    if disc > 0:
        print(disc)
        x1 = (-b - sqrt(disc))/(2*a)
        x2 = (-b + sqrt(disc))/(2*a)
        return x1, x2
    
    elif disc < 0:
        print("L'equation n'a pas de solution dans R")
    else:
        x0 = -b/(2*a)
        return x0