class Participant:
    def __init__(self, nom, prenoms, sexe, age, pays, niveau):
        self.nom = nom
        self.prenoms = prenoms
        self.sexe = sexe
        self.age = age
        self.pays = pays
        self.niveau = niveau


    def jeMePresente(self):
        print(f"Je m'appelle {self.nom} 
                {self.prenoms}, j'ai {self.age} ans et je suis {self.niveau} en Python.
                \nAh j'ai oublie, je viens du {self.pays}"
        )


part1 = Participant("PELEI", "Louise", "F", 23, "TOGO", "Experte")
part2 = Participant("AYOLA", "Timothe", "M", 29, "GHANA", "Expert")
part3 = Participant("AMETANA", "Edoh", "M", 18, "TOGO", "Expert")

part3.jeMePresente()

# print(part1.nom)
# part1.adresse = "123 Rue Abalo Koffi"
# print(part1.adresse)

# print(part2.adresse)