import pandas as pd
import matplotlib.pyplot as plt

# Essayons de lire le contenu du fichier

etudiant_df = pd.read_csv("etudiants.csv")
#etudiant_df = pd.read_csv("etudiants-1.csv")

counts = etudiant_df["Sexe"].value_counts()

#plt.bar(counts.index, counts.values)
plt.pie(counts, labels=["Hommes", "Femmes"])
#counts.plot(kind="pie")
#counts.plot(kind="bar")

plt.show()


