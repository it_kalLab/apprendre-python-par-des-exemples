import pandas as pd

# Essayons de lire le contenu du fichier

etudiant_df = pd.read_csv("etudiants.csv")
#etudiant_df = pd.read_csv("etudiants-1.csv")

# Faisons un filtre sur toutes les Femme qui ont la moyenne
# Elle a la moyenne si elle a une moyenne superieur ou egale a 10
femmes_avec_moyenne = etudiant_df[(etudiant_df["Sexe"]=="Femme") & (etudiant_df["Moyenne"] >= 10)]

#print(femmes_avec_moyenne)

noms_femmes_plus_20_moyenne = etudiant_df.loc[etudiant_df["Age"] > 20, "Nom"]

#print(noms_femmes_plus_20_moyenne.head())