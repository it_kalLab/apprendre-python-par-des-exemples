import pandas as pd

# Essayons de lire le contenu du fichier

etudiant_df = pd.read_csv("etudiants.csv")

# Essayons juste d'acceder a la colonne Age de notre fichier CSV
ages_series = etudiant_df["Age"]
moyennes_series = etudiant_df["Moyenne"]

# Cherchons la moyenne la plus faible et la moyenne la plus forte
print(f"La moyenne la plus faible est:  {moyennes_series.min()}")
print(f"La moyenne la plus forte est:  {moyennes_series.max()}")

# Cherchons le plus jeune et le plus vieux en age
print(f"Le plus jeune a  {ages_series.min()} ans")
print(f"Le plus vieux a  {ages_series.max()} ans")

# Essayons juste de selectionner le Nom, l'Age et le Sexe dans notre fichier CSV
nom_age_sexe = etudiant_df[["Nom", "Age", "Sexe"]]

# Faisons un filtre sur  seulement ceux qui ont plus de 20 ans
plus_20_df = nom_age_sexe[nom_age_sexe["Age"] > 20]
print(plus_20_df.shape)

# Verifions si notre donnees est correcte
# Cherchons le plus jeune et le plus vieux en age
print(f"Le plus jeune a  {plus_20_df['Age'].min()} ans")
print(f"Le plus vieux a  {plus_20_df['Age'].max()} ans")


# Faisons un filtre sur  seulement ceux qui ont moins de 20 ans
moins_20_df = nom_age_sexe[nom_age_sexe["Age"] < 20]
print(moins_20_df.shape)

# Verifions si notre donnees est correcte
# Cherchons le plus jeune et le plus vieux en age
print(f"Le plus jeune a  {moins_20_df['Age'].min()} ans")
print(f"Le plus vieux a  {moins_20_df['Age'].max()} ans")


# Faisons un filtre sur  seulement ceux qui ont 20 ans
ont_20_df = nom_age_sexe[nom_age_sexe["Age"] == 20]
print(ont_20_df.shape)

# Verifions si notre donnees est correcte
# Cherchons le plus jeune et le plus vieux en age
print(f"Le plus jeune a  {ont_20_df['Age'].min()} ans")
print(f"Le plus vieux a  {ont_20_df['Age'].max()} ans")