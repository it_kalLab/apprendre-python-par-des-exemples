### Chapitre 5 : Modules, Bibliothèques et Projets

## Objectif : Utiliser des modules et bibliothèques externes, et développer des projets simples

### 1. Modules et Packages

Les modules et packages permettent de structurer votre code Python en le séparant en fichiers et répertoires logiques.

#### Importer des Modules Intégrés

Python inclut de nombreux modules standard que vous pouvez utiliser sans installation supplémentaire.

**Exemple avec le module `math` :**
```python
import math

print(math.sqrt(16))  # 4.0
print(math.pi)        # 3.141592653589793
```

**Exemple avec le module `datetime` :**
```python
from datetime import datetime

now = datetime.now()
print(now)  # Affiche la date et l'heure actuelles
```

**Exemple avec le module `os` :**
```python
import os

print(os.listdir("."))  # Affiche la liste des fichiers dans le répertoire courant
```

#### Créer Vos Propres Modules

Vous pouvez créer vos propres modules en sauvegardant des fonctions dans un fichier avec l'extension `.py`.

**Exemple :**

Créez un fichier `mymodule.py` :
```python
# mymodule.py
def greet(name):
    return f"Hello, {name}!"
```

Importez et utilisez ce module dans un autre script :
```python
import mymodule

print(mymodule.greet("Alice"))  # Hello, Alice!
```

### 2. Bibliothèques Externes

#### Utiliser `pip` pour Installer des Bibliothèques

`pip` est l'outil de gestion de paquets de Python. Vous pouvez installer des bibliothèques externes en utilisant `pip`.

**Exemple :**
```sh
pip install requests
pip install pandas
pip install matplotlib
```

#### Introduction à des Bibliothèques Courantes

**`requests` pour les requêtes HTTP :**
```python
import requests

response = requests.get('https://api.github.com')
print(response.status_code)  # 200
print(response.json())  # Affiche les données de l'API
```

**`pandas` pour l'analyse de données :**
```python
import pandas as pd

data = {
    'Name': ['Alice', 'Bob', 'Charlie'],
    'Age': [25, 30, 35]
}
df = pd.DataFrame(data)
print(df)
```

**`matplotlib` pour les graphiques :**
```python
import matplotlib.pyplot as plt

x = [1, 2, 3, 4, 5]
y = [10, 20, 25, 30, 35]

plt.plot(x, y)
plt.xlabel('x-axis')
plt.ylabel('y-axis')
plt.title('Sample Plot')
plt.show()
```

### 3. Projets Pratiques

#### Projets Simples

1. **Jeu de Devinettes :**
```python
import random

number_to_guess = random.randint(1, 100)
guess = None

while guess != number_to_guess:
    guess = int(input("Devinez le nombre entre 1 et 100 : "))
    if guess < number_to_guess:
        print("Trop bas !")
    elif guess > number_to_guess:
        print("Trop haut !")
    else:
        print("Bravo, vous avez trouvé le nombre !")
```

2. **Mini Calculateur :**
```python
def add(a, b):
    return a + b

def subtract(a, b):
    return a - b

def multiply(a, b):
    return a * b

def divide(a, b):
    if b != 0:
        return a / b
    else:
        return "Erreur : Division par zéro"

print("Sélectionnez l'opération :")
print("1. Addition")
print("2. Soustraction")
print("3. Multiplication")
print("4. Division")

choice = input("Entrez le choix (1/2/3/4) : ")
num1 = float(input("Entrez le premier nombre : "))
num2 = float(input("Entrez le second nombre : "))

if choice == '1':
    print(f"Résultat : {add(num1, num2)}")
elif choice == '2':
    print(f"Résultat : {subtract(num1, num2)}")
elif choice == '3':
    print(f"Résultat : {multiply(num1, num2)}")
elif choice == '4':
    print(f"Résultat : {divide(num1, num2)}")
else:
    print("Choix invalide")
```

3. **Analyseur de Texte :**
```python
def word_count(text):
    words = text.split()
    return len(words)

def char_count(text):
    return len(text)

text = input("Entrez le texte à analyser : ")
print(f"Nombre de mots : {word_count(text)}")
print(f"Nombre de caractères : {char_count(text)}")
```

#### Développement d'un Petit Site Web avec Flask

Flask est un micro-framework pour développer des applications web en Python.

**Installation :**
```sh
pip install Flask
```

**Exemple de site web avec Flask :**
```python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def home():
    return "Bienvenue sur mon site web !"

@app.route('/about')
def about():
    return "À propos de nous."

if __name__ == '__main__':
    app.run(debug=True)
```

En exécutant ce script, vous pouvez accéder à votre site web à l'adresse `http://127.0.0.1:5000/`.

### Exercices
Trouvez ici les [exercices](Exercices_chap_5.md)

### Conclusion

Ce chapitre vous a introduit aux modules et packages, à l'utilisation de bibliothèques externes, et au développement de projets pratiques. En comprenant ces concepts et en les appliquant à travers des exemples concrets, vous serez en mesure de développer des applications Python plus sophistiquées et de gérer des projets plus complexes.
