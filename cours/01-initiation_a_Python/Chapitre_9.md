### Chapitre 9: Projets Pratiques

## Objectif : Appliquer les connaissances acquises à des projets concrets et apprendre à déployer des applications

### 19. Projets Pratiques

#### Création d'une Application de Gestion de Tâches

Ce projet vise à créer une application simple pour gérer des tâches, permettant d'ajouter, de supprimer et de marquer des tâches comme terminées.

**Exemple :**
1. **Structure de base :**
    ```python
    class Task:
        def __init__(self, name):
            self.name = name
            self.completed = False

        def complete(self):
            self.completed = True

        def __str__(self):
            return f"{'[X]' if self.completed else '[ ]'} {self.name}"

    tasks = []

    def add_task(name):
        task = Task(name)
        tasks.append(task)

    def remove_task(index):
        if 0 <= index < len(tasks):
            tasks.pop(index)

    def complete_task(index):
        if 0 <= index < len(tasks):
            tasks[index].complete()

    # Ajouter quelques tâches
    add_task("Faire les courses")
    add_task("Rédiger un rapport")

    # Afficher les tâches
    for task in tasks:
        print(task)

    # Marquer la première tâche comme terminée
    complete_task(0)

    # Afficher les tâches
    for task in tasks:
        print(task)
    ```

#### Développement d'un Jeu Simple

Créer un jeu simple comme le Snake ou le Tic-Tac-Toe permet de pratiquer la logique de programmation et la manipulation des graphiques.

**Exemple : Jeu Tic-Tac-Toe**
```python
def print_board(board):
    for row in board:
        print(" | ".join(row))
        print("-" * 5)

def check_winner(board, player):
    # Vérifier les lignes, colonnes et diagonales
    for row in board:
        if all(s == player for s in row):
            return True
    for col in range(3):
        if all(row[col] == player for row in board):
            return True
    if all(board[i][i] == player for i in range(3)) or all(board[i][2 - i] == player for i in range(3)):
        return True
    return False

board = [[" " for _ in range(3)] for _ in range(3)]
current_player = "X"

for _ in range(9):
    print_board(board)
    row, col = map(int, input(f"Joueur {current_player}, entrez votre mouvement (row col) : ").split())
    if board[row][col] == " ":
        board[row][col] = current_player
        if check_winner(board, current_player):
            print_board(board)
            print(f"Joueur {current_player} gagne!")
            break
        current_player = "O" if current_player == "X" else "X"
    else:
        print("Cette case est déjà occupée.")

print("Match nul!")
```

#### Analyse de Données avec `pandas` et `matplotlib`

Apprendre à utiliser des bibliothèques pour analyser et visualiser des données est essentiel pour les projets de science des données.

**Exemple :**
```python
import pandas as pd
import matplotlib.pyplot as plt

# Chargement des données
data = {
    'Nom': ['Alice', 'Bob', 'Charlie'],
    'Âge': [25, 30, 35],
    'Score': [85, 90, 95]
}
df = pd.DataFrame(data)

# Analyse des données
print(df.describe())

# Visualisation des données
plt.bar(df['Nom'], df['Score'])
plt.xlabel('Nom')
plt.ylabel('Score')
plt.title('Scores des participants')
plt.show()
```

### 20. Déploiement d'Applications

#### Introduction à la Création de Sites Web avec Django

Django est un framework web Python permettant de développer des applications web rapidement et efficacement.

**Exemple :**
```sh
# Installation de Django
pip install django

# Création d'un nouveau projet Django
django-admin startproject mysite

# Démarrage du serveur de développement
cd mysite
python manage.py runserver
```

**Structure de base d'une vue Django :**
```python
# mysite/views.py
from django.http import HttpResponse

def home(request):
    return HttpResponse("Bienvenue sur mon site Django")

# mysite/urls.py
from django.contrib import admin
from django.urls import path
from .views import home

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home),
]
```

#### Déploiement sur Ubuntu

Le déploiement sur un serveur Ubuntu permet de rendre votre application accessible au public.

**Exemple :**
1. **Mise à jour du serveur :**
    ```sh
    sudo apt update && sudo apt upgrade -y
    ```

2. **Installation des dépendances :**
    ```sh
    sudo apt install python3-pip python3-venv nginx
    ```

3. **Configuration de l'application Django :**
    ```sh
    cd /path/to/your/django/project
    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt
    python manage.py collectstatic
    ```

4. **Configuration de Nginx :**
    ```sh
    sudo nano /etc/nginx/sites-available/mysite

    # Ajouter la configuration suivante :
    server {
        listen 80;
        server_name your_domain_or_IP;

        location / {
            proxy_pass http://127.0.0.1:8000;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }
    }

    sudo ln -s /etc/nginx/sites-available/mysite /etc/nginx/sites-enabled
    sudo systemctl restart nginx
    ```

5. **Exécution de l'application avec `gunicorn` :**
    ```sh
    gunicorn --workers 3 mysite.wsgi:application
    ```

### Conclusion

Ce chapitre a couvert la réalisation de projets pratiques en Python, comme la création d'une application de gestion de tâches, le développement d'un jeu simple et l'analyse de données. Il a également abordé l'introduction à la création de sites web avec Django et le déploiement sur un serveur Ubuntu, fournissant une vue d'ensemble complète de l'utilisation de Python dans des applications réelles.
