# Ressources
Voici quelques ressources en français qui vous aideront dans l'apprentissage du langage Python :

## Livres :

1. **"Apprenez à programmer en Python" par Gérard Swinnen :** Ce livre est une référence pour les débutants. Il couvre les bases du langage Python de manière détaillée avec de nombreux exemples et exercices.

2. **"Python pour les kids" par Jason R. Briggs :** Conçu spécialement pour les enfants, ce livre rend l'apprentissage de Python ludique et accessible grâce à des explications simples et des projets amusants.

3. **"Python 3 - Les fondamentaux du langage" par Gérard Swinnen et Tarek Ziadé :** Un autre livre de Gérard Swinnen, qui offre une approche complète du langage Python, en couvrant les bases ainsi que des sujets avancés.

## Plateformes en Ligne :

1. **OpenClassrooms :** OpenClassrooms propose plusieurs parcours et cours en ligne sur Python, adaptés à tous les niveaux, du débutant à l'expert.

2. **Udemy :** Udemy propose une variété de cours en français sur Python, enseignés par des instructeurs qualifiés. Vous pouvez trouver des cours pour tous les niveaux, du débutant à l'avancé.

3. **Coursera :** Coursera propose des cours en français sur Python, dispensés par des universités renommées. Vous pouvez suivre ces cours gratuitement ou choisir de payer pour obtenir un certificat.

4. **Codecademy :** Bien que Codecademy soit principalement en anglais, il propose également des cours interactifs sur Python qui sont accessibles aux francophones.

5. **Le Wagon :** Le Wagon propose des formations intensives en développement web, y compris des modules sur Python, dans plusieurs villes francophones à travers le monde.

Ces ressources vous offriront une variété d'options pour apprendre Python selon votre style préféré d'apprentissage et votre niveau d'expertise actuel.
