# Chapitre 7 : Avancé et Pratique Continue

## Objectif : Approfondir les connaissances et travailler sur des projets plus complexes

### 1. Gestion des Exceptions

Les exceptions permettent de gérer les erreurs de manière élégante sans interrompre l'exécution du programme.

#### `try`, `except`, `finally`

Le bloc `try` permet d'exécuter du code tout en capturant les exceptions potentielles. Le bloc `finally` s'exécute toujours, qu'il y ait une exception ou non.

**Exemple :**
```python
try:
    number = int(input("Entrez un nombre : "))
    result = 10 / number
    print(f"Le résultat est {result}")
except ValueError:
    print("Ce n'est pas un nombre valide.")
except ZeroDivisionError:
    print("Division par zéro interdite.")
finally:
    print("Cette phrase s'exécute toujours.")
```

#### Création de Vos Propres Exceptions

Vous pouvez créer des exceptions personnalisées en héritant de la classe `Exception`.

**Exemple :**
```python
class NegativeNumberError(Exception):
    """Exception levée lorsque le nombre est négatif."""
    pass

def check_positive(number):
    if number < 0:
        raise NegativeNumberError("Le nombre ne doit pas être négatif.")
    return number

try:
    num = int(input("Entrez un nombre positif : "))
    check_positive(num)
    print(f"Le nombre est {num}")
except NegativeNumberError as e:
    print(e)
```

### 2. Générateurs et Itérateurs

Les générateurs et itérateurs permettent de travailler efficacement avec de grandes quantités de données.

#### Comprendre les Itérateurs

Un itérateur est un objet qui implémente les méthodes `__iter__()` et `__next__()`.

**Exemple :**
```python
class MyIterator:
    def __init__(self, limit):
        self.limit = limit
        self.counter = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.counter < self.limit:
            self.counter += 1
            return self.counter
        else:
            raise StopIteration

for number in MyIterator(5):
    print(number)  # Affiche 1, 2, 3, 4, 5
```

#### Utilisation des Générateurs (`yield`)

Les générateurs simplifient la création d'itérateurs en utilisant le mot-clé `yield`.

**Exemple :**
```python
def my_generator(limit):
    counter = 0
    while counter < limit:
        counter += 1
        yield counter

for number in my_generator(5):
    print(number)  # Affiche 1, 2, 3, 4, 5
```

### 3. Travail sur des Projets Plus Complexes

#### Applications de Gestion de Bases de Données avec SQLite et SQLAlchemy

SQLite est une base de données légère intégrée à Python, et SQLAlchemy est une bibliothèque ORM (Object Relational Mapper).

**Exemple :**
```python
import sqlite3

# Connexion à la base de données
conn = sqlite3.connect('example.db')
cursor = conn.cursor()

# Création d'une table
cursor.execute('''CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, name TEXT)''')

# Insertion de données
cursor.execute("INSERT INTO users (name) VALUES ('Alice')")
cursor.execute("INSERT INTO users (name) VALUES ('Bob')")
conn.commit()

# Lecture de données
cursor.execute("SELECT * FROM users")
for row in cursor.fetchall():
    print(row)

conn.close()
```

#### Analyse de Données avec `pandas` et Visualisation avec `matplotlib` ou `seaborn`

**Exemple :**
```python
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# Chargement des données
data = {
    'Name': ['Alice', 'Bob', 'Charlie'],
    'Age': [25, 30, 35],
    'Score': [85, 90, 95]
}
df = pd.DataFrame(data)

# Analyse de données
print(df.describe())

# Visualisation avec matplotlib
df.plot(kind='bar', x='Name', y='Score')
plt.show()

# Visualisation avec seaborn
sns.barplot(x='Name', y='Score', data=df)
plt.show()
```

### 4. Contribuer à des Projets Open Source

Contribuer à des projets open source est un excellent moyen de mettre en pratique vos compétences et de collaborer avec d'autres développeurs.

#### Utiliser GitLab pour Collaborer

**Exemple :**
1. **Cloner un projet :**
    ```sh
    git clone https://gitlab.com/username/project.git
    cd project
    ```

2. **Faire des changements et les committer :**
    ```sh
    git checkout -b my-feature-branch
    # Modifiez les fichiers nécessaires
    git add .
    git commit -m "Ajout de ma nouvelle fonctionnalité"
    ```

3. **Pousser les changements et créer une Pull Request :**
    ```sh
    git push origin my-feature-branch
    # Ouvrez GitLab et créez une Merge Request
    ```

#### Comprendre le Workflow Git (clone, commit, push, pull request)

- **Clone** : Copier un dépôt distant localement.
- **Commit** : Enregistrer des changements dans le dépôt local.
- **Push** : Envoyer les commits locaux vers le dépôt distant.
- **Pull Request/Merge Request** : Demander l'intégration de vos changements dans le dépôt principal.

**Exemple :**
```sh
# Cloner le dépôt
git clone https://gitlab.com/username/project.git

# Créer une nouvelle branche
git checkout -b new-feature

# Apporter des modifications, puis les ajouter et committer
git add .
git commit -m "Ajout de nouvelles fonctionnalités"

# Pousser la branche et créer une Pull Request
git push origin new-feature
```

### Conclusion

Ce chapitre a couvert des concepts avancés de Python tels que la gestion des exceptions, les générateurs et itérateurs, ainsi que des projets plus complexes incluant la gestion de bases de données et l'analyse de données. Vous avez également appris à contribuer à des projets open source, ce qui vous aidera à collaborer efficacement avec d'autres développeurs. Ces compétences sont essentielles pour devenir un développeur Python accompli.