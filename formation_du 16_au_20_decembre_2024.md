# Programme de formation

## Jour 1 : 
- 08h00 à 08h30 : Accueil et enregistrement
- 08h30 à 09h30 : Présentation du CEA-SMIA et de ses partenaires
- 09h30 à 10h30 : 
    - Installation des outils (Python et Visual Studio Code)
    - Introduction au language Python
    - Syntaxe de base (notion de variables)
    - Exercices

- 10h30 à 11h00 : Pause café

- 11h00 à 14h00 : 
    - Structures de Contrôle (if elif)
    - Exercices
    - Notion d'environnement virtuel

- 14h00 à   -   : Déjeuner et départ 

---

## Jour 2 : 
- 08h00 à 08h30 : Révision
- 08h30 à 10h30 : 
    - Syntaxe de base (silicing, list, dictionnaire, tuple)
    - Exercices
    - Structures de Contrôle (for, while)
    - Exercices
    - Fonctions
    - Exercices


- 10h30 à 11h00 : Pause café
- 11h00 à 14h00 : 
    - Concepts de la Programmation Orientée Objet (classes)
    - Exercices

- 14h00 à   -   : Déjeuner et départ

---

## Jour 3 : 
- 08h00 à 08h30 : Révision 
- 08h30 à 10h30 : 
    - Programmation Orientée Objet (classes, objets et héritage)
    - Exercices
    
- 10h30 à 11h00 : Pause café
- 11h00 à 14h00 : 
    - Introduction à l'analyse de données
    - Installation de la distribution Anaconda et d'un environnement virtuel
    - Installation des paquages (pandas, matplotlib)
    - Exploration et extraction de la donnée 
- 14h00 à   -   : Déjeuner et départ

---

## Jour 4 : 
- 08h00 à 08h30 : Révision
- 08h30 à 10h30 : 
    - Analyse de données avec Pandas et génération de graphiques avec Matplotlib (pie et bar)
    - Exercices
    - Génération et interprétation des graphiques fait avec Seaborn
    - Exercices
- 10h30 à 11h00 : Pause café
- 11h00 à 14h00 : Initiation au web avec le framework Django
- 14h00 à   -   : Déjeuner et départ

---

## Jour 5 :
- 08h00 à 08h30 : Révision
- 08h30 à 09h30 : Préseantation de la plateforme kapple et des quelques jeux de données
- 09h30 à 10h30 : Analyse du jeu de données portant sur Titanic
- 10h30 à 11h00 : Pause café
- 11h00 à 14h00 : Analyse du jeu de données portant sur Titanic
- 14h00 à 14h30 : Remise des attestations
- 14h00 à   -   : Déjeuner et départ
