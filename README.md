# Apprendre python par des exemples



## Plan du cours
1. [Chapitre 1](Chapitre_0.md)
2. [Chapitre 2](Chapitre_1.md)
3. [Chapitre 3](Chapitre_2.md)
4. [Chapitre 4](Chapitre_3.md)
5. [Chapitre 5](Chapitre_4.md)
6. [Chapitre 6](Chapitre_5.md)
7. [Chapitre 7](Chapitre_6.md)
8. [Chapitre 8](Chapitre_7.md)
9. [Chapitre 9](Chapitre_8.md)
10. [Chapitre 10](Chapitre_9.md)
11. [Chapitre 11](Chapitre_10.md)



# Comment cloner un projet ?

```
cd existing_repo
git remote add origin https://gitlab.com/it_kalLab/apprendre-python-par-des-exemples.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools
<!--
- [ ] [Set up project integrations](https://gitlab.com/it_kalLab/apprendre-python-par-des-exemples/-/settings/integrations)
-->
